/*
 Navicat Premium Data Transfer

 Source Server         : 110服务器MySQL
 Source Server Type    : MySQL
 Source Server Version : 80024
 Source Host           : 110.42.134.216:3306
 Source Schema         : tsingeye-viid

 Target Server Type    : MySQL
 Target Server Version : 80024
 File Encoding         : 65001

 Date: 07/09/2022 14:55:14
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for gen_table
-- ----------------------------
DROP TABLE IF EXISTS `gen_table`;
CREATE TABLE `gen_table`  (
  `table_id` bigint(0) NOT NULL AUTO_INCREMENT COMMENT '编号',
  `table_name` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '' COMMENT '表名称',
  `table_comment` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '' COMMENT '表描述',
  `sub_table_name` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '关联子表的表名',
  `sub_table_fk_name` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '子表关联的外键名',
  `class_name` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '' COMMENT '实体类名称',
  `tpl_category` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT 'crud' COMMENT '使用的模板（crud单表操作 tree树表操作）',
  `package_name` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '生成包路径',
  `module_name` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '生成模块名',
  `business_name` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '生成业务名',
  `function_name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '生成功能名',
  `function_author` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '生成功能作者',
  `gen_type` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '0' COMMENT '生成代码方式（0zip压缩包 1自定义路径）',
  `gen_path` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '/' COMMENT '生成路径（不填默认项目路径）',
  `options` varchar(1000) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '其它生成选项',
  `create_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`table_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 9 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci COMMENT = '代码生成业务表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of gen_table
-- ----------------------------
INSERT INTO `gen_table` VALUES (1, 'sys_model_warehouse', '数据仓库', NULL, NULL, 'SysModelWarehouse', 'crud', 'com.ruoyi.system', 'system', 'warehouse', '数据仓库', 'ruoyi', '0', '/', NULL, 'admin', '2022-04-18 17:19:55', '', NULL, NULL);
INSERT INTO `gen_table` VALUES (2, 'viid_face', '人脸信息', NULL, NULL, 'ViidFace', 'crud', 'com.tsingeye.viid', 'viid', 'face', '人脸信息', '姜风', '0', '/', '{\"parentMenuId\":\"2007\"}', 'admin', '2022-04-21 16:03:54', '', '2022-04-21 16:07:38', NULL);
INSERT INTO `gen_table` VALUES (3, 'viid_face_image', '人脸照片', NULL, NULL, 'ViidFaceImage', 'crud', 'com.tsingeye.viid', 'viid', 'image', '人脸照片', '姜风', '0', '/', '{\"parentMenuId\":\"2007\"}', 'admin', '2022-04-21 16:03:55', '', '2022-04-21 16:46:33', NULL);
INSERT INTO `gen_table` VALUES (4, 'viid_device', '设备信息', NULL, NULL, 'ViidDevice', 'crud', 'com.tsingeye.viid', 'viid', 'device', '设备信息', '姜风', '0', '/', '{\"parentMenuId\":\"2020\"}', 'admin', '2022-04-22 11:28:35', '', '2022-04-22 11:33:34', NULL);
INSERT INTO `gen_table` VALUES (5, 'viid_motor_vehicle', '机动车信息', NULL, NULL, 'ViidMotorVehicle', 'crud', 'com.tsingeye.viid', 'viid', 'vehicle', '机动车信息', '姜风', '0', '/', '{\"parentMenuId\":\"2027\"}', 'admin', '2022-06-27 16:01:46', '', '2022-06-27 16:25:32', NULL);
INSERT INTO `gen_table` VALUES (6, 'viid_motor_vehicle_image', '机动车图片', NULL, NULL, 'ViidMotorVehicleImage', 'crud', 'com.tsingeye.viid', 'viid', 'vehicleImage', '机动车图片', '姜风', '0', '/', '{\"parentMenuId\":\"2027\"}', 'admin', '2022-06-27 16:01:51', '', '2022-06-27 16:55:33', NULL);
INSERT INTO `gen_table` VALUES (7, 'viid_person', '人体信息', NULL, NULL, 'ViidPerson', 'crud', 'com.tsingeye.viid', 'viid', 'person', '人体信息', '姜风', '0', '/', '{\"parentMenuId\":\"2040\"}', 'admin', '2022-06-29 10:47:08', '', '2022-06-29 11:00:44', NULL);
INSERT INTO `gen_table` VALUES (8, 'viid_person_image', '人体图片', NULL, NULL, 'ViidPersonImage', 'crud', 'com.tsingeye.viid', 'viid', 'personImage', '人体图片', '姜风', '0', '/', '{\"parentMenuId\":\"2040\"}', 'admin', '2022-06-29 10:47:14', '', '2022-06-29 10:49:16', NULL);

-- ----------------------------
-- Table structure for gen_table_column
-- ----------------------------
DROP TABLE IF EXISTS `gen_table_column`;
CREATE TABLE `gen_table_column`  (
  `column_id` bigint(0) NOT NULL AUTO_INCREMENT COMMENT '编号',
  `table_id` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '归属表编号',
  `column_name` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '列名称',
  `column_comment` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '列描述',
  `column_type` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '列类型',
  `java_type` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT 'JAVA类型',
  `java_field` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT 'JAVA字段名',
  `is_pk` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '是否主键（1是）',
  `is_increment` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '是否自增（1是）',
  `is_required` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '是否必填（1是）',
  `is_insert` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '是否为插入字段（1是）',
  `is_edit` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '是否编辑字段（1是）',
  `is_list` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '是否列表字段（1是）',
  `is_query` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '是否查询字段（1是）',
  `query_type` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT 'EQ' COMMENT '查询方式（等于、不等于、大于、小于、范围）',
  `html_type` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '显示类型（文本框、文本域、下拉框、复选框、单选框、日期控件）',
  `dict_type` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '' COMMENT '字典类型',
  `sort` int(0) NULL DEFAULT NULL COMMENT '排序',
  `create_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`column_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 255 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci COMMENT = '代码生成业务表字段' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of gen_table_column
-- ----------------------------
INSERT INTO `gen_table_column` VALUES (1, '1', 'id', NULL, 'bigint', 'Long', 'id', '1', '0', NULL, '1', NULL, NULL, NULL, 'EQ', 'input', '', 1, 'admin', '2022-04-18 17:19:55', '', NULL);
INSERT INTO `gen_table_column` VALUES (2, '1', 'model_name', '模型名称', 'varchar(255)', 'String', 'modelName', '0', '0', '1', '1', '1', '1', '1', 'LIKE', 'input', '', 2, 'admin', '2022-04-18 17:19:55', '', NULL);
INSERT INTO `gen_table_column` VALUES (3, '1', 'model_address', '模型地址', 'varchar(512)', 'String', 'modelAddress', '0', '0', '1', '1', '1', '1', '1', 'EQ', 'textarea', '', 3, 'admin', '2022-04-18 17:19:55', '', NULL);
INSERT INTO `gen_table_column` VALUES (4, '1', 'create_time', '上传时间', 'datetime', 'Date', 'createTime', '0', '0', NULL, '1', NULL, NULL, NULL, 'EQ', 'datetime', '', 4, 'admin', '2022-04-18 17:19:55', '', NULL);
INSERT INTO `gen_table_column` VALUES (5, '1', 'create_by', '创建人', 'varchar(64)', 'String', 'createBy', '0', '0', NULL, '1', NULL, NULL, NULL, 'EQ', 'input', '', 5, 'admin', '2022-04-18 17:19:55', '', NULL);
INSERT INTO `gen_table_column` VALUES (6, '1', 'update_time', '更新时间', 'datetime', 'Date', 'updateTime', '0', '0', NULL, '1', '1', NULL, NULL, 'EQ', 'datetime', '', 6, 'admin', '2022-04-18 17:19:55', '', NULL);
INSERT INTO `gen_table_column` VALUES (7, '1', 'update_by', '更新人', 'varchar(64)', 'String', 'updateBy', '0', '0', NULL, '1', '1', NULL, NULL, 'EQ', 'input', '', 7, 'admin', '2022-04-18 17:19:55', '', NULL);
INSERT INTO `gen_table_column` VALUES (8, '1', 'deleted', '0是未删除 1是删除', 'int', 'Long', 'deleted', '0', '0', '1', '1', '1', '1', '1', 'EQ', 'input', '', 8, 'admin', '2022-04-18 17:19:55', '', NULL);
INSERT INTO `gen_table_column` VALUES (9, '1', 'remark', '备注', 'varchar(1024)', 'String', 'remark', '0', '0', NULL, '1', '1', '1', NULL, 'EQ', 'textarea', '', 9, 'admin', '2022-04-18 17:19:55', '', NULL);
INSERT INTO `gen_table_column` VALUES (10, '2', 'id', '主键', 'varchar(255)', 'String', 'id', '1', '0', NULL, '1', NULL, NULL, NULL, 'EQ', 'input', '', 1, 'admin', '2022-04-21 16:03:54', '', '2022-04-21 16:07:38');
INSERT INTO `gen_table_column` VALUES (11, '2', 'face_id', '人脸标识', 'varchar(255)', 'String', 'faceId', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 2, 'admin', '2022-04-21 16:03:54', '', '2022-04-21 16:07:38');
INSERT INTO `gen_table_column` VALUES (12, '2', 'info_kind', '信息分类：人工采集、自动采集', 'varchar(255)', 'String', 'infoKind', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 3, 'admin', '2022-04-21 16:03:54', '', '2022-04-21 16:07:38');
INSERT INTO `gen_table_column` VALUES (13, '2', 'source_id', '来源标识：来源图像信息标识', 'varchar(255)', 'String', 'sourceId', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 4, 'admin', '2022-04-21 16:03:55', '', '2022-04-21 16:07:38');
INSERT INTO `gen_table_column` VALUES (14, '2', 'device_id', '设备编码', 'varchar(255)', 'String', 'deviceId', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 5, 'admin', '2022-04-21 16:03:55', '', '2022-04-21 16:07:38');
INSERT INTO `gen_table_column` VALUES (15, '2', 'shot_time', '拍摄时间', 'datetime', 'Date', 'shotTime', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'datetime', '', 6, 'admin', '2022-04-21 16:03:55', '', '2022-04-21 16:07:38');
INSERT INTO `gen_table_column` VALUES (16, '2', 'left_top_x', '左上角X坐标', 'varchar(255)', 'String', 'leftTopX', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 7, 'admin', '2022-04-21 16:03:55', '', '2022-04-21 16:07:38');
INSERT INTO `gen_table_column` VALUES (17, '2', 'left_top_y', '左上角Y坐标', 'varchar(255)', 'String', 'leftTopY', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 8, 'admin', '2022-04-21 16:03:55', '', '2022-04-21 16:07:39');
INSERT INTO `gen_table_column` VALUES (18, '2', 'right_btm_x', '右下角X坐标', 'varchar(255)', 'String', 'rightBtmX', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 9, 'admin', '2022-04-21 16:03:55', '', '2022-04-21 16:07:39');
INSERT INTO `gen_table_column` VALUES (19, '2', 'right_btm_y', '右下角Y坐标', 'varchar(255)', 'String', 'rightBtmY', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 10, 'admin', '2022-04-21 16:03:55', '', '2022-04-21 16:07:39');
INSERT INTO `gen_table_column` VALUES (20, '2', 'location_mark_time', '位置标记时间', 'datetime', 'Date', 'locationMarkTime', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'datetime', '', 11, 'admin', '2022-04-21 16:03:55', '', '2022-04-21 16:07:39');
INSERT INTO `gen_table_column` VALUES (21, '2', 'face_appear_time', '人脸出现时间', 'datetime', 'Date', 'faceAppearTime', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'datetime', '', 12, 'admin', '2022-04-21 16:03:55', '', '2022-04-21 16:07:39');
INSERT INTO `gen_table_column` VALUES (22, '2', 'face_dis_appear_time', '人脸消失时间', 'datetime', 'Date', 'faceDisAppearTime', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'datetime', '', 13, 'admin', '2022-04-21 16:03:55', '', '2022-04-21 16:07:39');
INSERT INTO `gen_table_column` VALUES (23, '2', 'gender_code', '性别代码', 'varchar(255)', 'String', 'genderCode', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 14, 'admin', '2022-04-21 16:03:55', '', '2022-04-21 16:07:39');
INSERT INTO `gen_table_column` VALUES (24, '2', 'age_up_limit', '年龄上限', 'varchar(255)', 'String', 'ageUpLimit', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 15, 'admin', '2022-04-21 16:03:55', '', '2022-04-21 16:07:39');
INSERT INTO `gen_table_column` VALUES (25, '2', 'age_lower_limit', '年龄下限', 'varchar(255)', 'String', 'ageLowerLimit', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 16, 'admin', '2022-04-21 16:03:55', '', '2022-04-21 16:07:39');
INSERT INTO `gen_table_column` VALUES (26, '2', 'glass_style', '眼镜款式', 'varchar(255)', 'String', 'glassStyle', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 17, 'admin', '2022-04-21 16:03:55', '', '2022-04-21 16:07:39');
INSERT INTO `gen_table_column` VALUES (27, '2', 'emotion', '不清楚字段含义', 'varchar(255)', 'String', 'emotion', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 18, 'admin', '2022-04-21 16:03:55', '', '2022-04-21 16:07:39');
INSERT INTO `gen_table_column` VALUES (28, '2', 'is_driver', '是否驾驶员 0：否、1：是、2：不确定', 'varchar(255)', 'String', 'isDriver', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 19, 'admin', '2022-04-21 16:03:55', '', '2022-04-21 16:07:39');
INSERT INTO `gen_table_column` VALUES (29, '2', 'is_foreigner', '是否涉外人员 0：否、1：是、2：不确定', 'varchar(255)', 'String', 'isForeigner', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 20, 'admin', '2022-04-21 16:03:55', '', '2022-04-21 16:07:39');
INSERT INTO `gen_table_column` VALUES (30, '2', 'is_suspected_terrorist', '是否涉恐人员 0：否、1：是、2：不确定', 'varchar(255)', 'String', 'isSuspectedTerrorist', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 21, 'admin', '2022-04-21 16:03:55', '', '2022-04-21 16:07:39');
INSERT INTO `gen_table_column` VALUES (31, '2', 'is_criminal_involved', '是否涉案人员 0：否、1：是、2：不确定', 'varchar(255)', 'String', 'isCriminalInvolved', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 22, 'admin', '2022-04-21 16:03:55', '', '2022-04-21 16:07:39');
INSERT INTO `gen_table_column` VALUES (32, '2', 'is_detainess', '是否在押人员 0：否、1：是、2：不确定', 'varchar(255)', 'String', 'isDetainess', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 23, 'admin', '2022-04-21 16:03:55', '', '2022-04-21 16:07:39');
INSERT INTO `gen_table_column` VALUES (33, '2', 'is_victim', '是否被害人 0：否、1：是、2：不确定', 'varchar(255)', 'String', 'isVictim', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 24, 'admin', '2022-04-21 16:03:55', '', '2022-04-21 16:07:39');
INSERT INTO `gen_table_column` VALUES (34, '2', 'is_suspicious_person', '是否可疑人员 0：否、1：是、2：不确定', 'varchar(255)', 'String', 'isSuspiciousPerson', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 25, 'admin', '2022-04-21 16:03:55', '', '2022-04-21 16:07:39');
INSERT INTO `gen_table_column` VALUES (35, '2', 'similaritydegree', '相似度[0,1]', 'varchar(255)', 'String', 'similaritydegree', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 26, 'admin', '2022-04-21 16:03:55', '', '2022-04-21 16:07:39');
INSERT INTO `gen_table_column` VALUES (36, '2', 'create_time', '创建时间', 'datetime', 'Date', 'createTime', '0', '0', NULL, '1', NULL, NULL, NULL, 'EQ', 'datetime', '', 27, 'admin', '2022-04-21 16:03:55', '', '2022-04-21 16:07:39');
INSERT INTO `gen_table_column` VALUES (37, '2', 'remark', '备注', 'varchar(255)', 'String', 'remark', '0', '0', NULL, '1', '1', '1', NULL, 'EQ', 'input', '', 28, 'admin', '2022-04-21 16:03:55', '', '2022-04-21 16:07:39');
INSERT INTO `gen_table_column` VALUES (38, '3', 'id', '编号', 'varchar(255)', 'String', 'id', '1', '0', NULL, '1', NULL, NULL, NULL, 'EQ', 'input', '', 1, 'admin', '2022-04-21 16:03:55', '', '2022-04-21 16:46:33');
INSERT INTO `gen_table_column` VALUES (39, '3', 'face_id', '人脸标识', 'varchar(255)', 'String', 'faceId', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 2, 'admin', '2022-04-21 16:03:55', '', '2022-04-21 16:46:33');
INSERT INTO `gen_table_column` VALUES (40, '3', 'image_id', '图像标识', 'varchar(255)', 'String', 'imageId', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 3, 'admin', '2022-04-21 16:03:55', '', '2022-04-21 16:46:33');
INSERT INTO `gen_table_column` VALUES (41, '3', 'event_sort', '事件分类：自动分析事件类型，设备采集必选', 'varchar(255)', 'String', 'eventSort', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 4, 'admin', '2022-04-21 16:03:55', '', '2022-04-21 16:46:33');
INSERT INTO `gen_table_column` VALUES (42, '3', 'device_id', '设备编码', 'varchar(255)', 'String', 'deviceId', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 5, 'admin', '2022-04-21 16:03:55', '', '2022-04-21 16:46:33');
INSERT INTO `gen_table_column` VALUES (43, '3', 'storage_path', '存储路径：图像文件的存储路径，采用URI命名规则', 'varchar(255)', 'String', 'storagePath', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 6, 'admin', '2022-04-21 16:03:55', '', '2022-04-21 16:46:33');
INSERT INTO `gen_table_column` VALUES (44, '3', 'type', '不清楚自动含义', 'varchar(255)', 'String', 'type', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'select', '', 7, 'admin', '2022-04-21 16:03:55', '', '2022-04-21 16:46:33');
INSERT INTO `gen_table_column` VALUES (45, '3', 'file_format', '图像文件格式', 'varchar(255)', 'String', 'fileFormat', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 8, 'admin', '2022-04-21 16:03:55', '', '2022-04-21 16:46:33');
INSERT INTO `gen_table_column` VALUES (46, '3', 'shot_time', '拍摄时间', 'datetime', 'Date', 'shotTime', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'datetime', '', 9, 'admin', '2022-04-21 16:03:55', '', '2022-04-21 16:46:33');
INSERT INTO `gen_table_column` VALUES (47, '3', 'width', '宽度', 'varchar(255)', 'String', 'width', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 10, 'admin', '2022-04-21 16:03:55', '', '2022-04-21 16:46:33');
INSERT INTO `gen_table_column` VALUES (48, '3', 'height', '高度', 'varchar(255)', 'String', 'height', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 11, 'admin', '2022-04-21 16:03:55', '', '2022-04-21 16:46:33');
INSERT INTO `gen_table_column` VALUES (49, '3', 'data', '图片数据，采用base64加密', 'varchar(1024)', 'String', 'data', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'textarea', '', 12, 'admin', '2022-04-21 16:03:55', '', '2022-04-21 16:46:33');
INSERT INTO `gen_table_column` VALUES (50, '3', 'dataBytes', 'byte[] 形式的base64图片', 'longblob', 'String', 'databytes', '0', '0', NULL, '1', '1', '1', '1', 'EQ', NULL, '', 13, 'admin', '2022-04-21 16:03:55', '', '2022-04-21 16:46:33');
INSERT INTO `gen_table_column` VALUES (51, '3', 'image_url', 'minio上传后的图片地址', 'varchar(255)', 'String', 'imageUrl', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 14, 'admin', '2022-04-21 16:03:55', '', '2022-04-21 16:46:33');
INSERT INTO `gen_table_column` VALUES (52, '3', 'create_time', '上传时间', 'datetime', 'Date', 'createTime', '0', '0', NULL, '1', NULL, NULL, NULL, 'EQ', 'datetime', '', 15, 'admin', '2022-04-21 16:03:55', '', '2022-04-21 16:46:33');
INSERT INTO `gen_table_column` VALUES (53, '4', 'id', '主键', 'bigint', 'String', 'id', '1', '0', NULL, '1', NULL, NULL, NULL, 'EQ', 'input', NULL, 1, 'admin', '2022-04-22 11:28:35', NULL, '2022-04-22 11:33:34');
INSERT INTO `gen_table_column` VALUES (54, '4', 'device_id', '设备id', 'varchar(255)', 'String', 'deviceId', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 2, 'admin', '2022-04-22 11:28:35', NULL, '2022-04-22 11:33:34');
INSERT INTO `gen_table_column` VALUES (55, '4', 'register_time', '最后一次注册时间', 'timestamp', 'String', 'registerTime', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'datetime', '', 3, 'admin', '2022-04-22 11:28:35', NULL, '2022-04-22 11:33:34');
INSERT INTO `gen_table_column` VALUES (56, '4', 'keep_alive_time', '最后一次保活时间', 'timestamp', 'String', 'keepAliveTime', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'datetime', '', 4, 'admin', '2022-04-22 11:28:35', NULL, '2022-04-22 11:33:34');
INSERT INTO `gen_table_column` VALUES (57, '4', 'status', '在线状态 1、在线 2、不在线', 'int', 'Integer', 'status', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'radio', '', 5, 'admin', '2022-04-22 11:28:35', NULL, '2022-04-22 11:33:34');
INSERT INTO `gen_table_column` VALUES (58, '4', 'enable', '启用状态 1、启用  2、不启用', 'int', 'Integer', 'enable', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 6, 'admin', '2022-04-22 11:28:35', NULL, '2022-04-22 11:33:34');
INSERT INTO `gen_table_column` VALUES (59, '4', 'ip', '设备ip', 'varchar(255)', 'String', 'ip', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 7, 'admin', '2022-04-22 11:28:35', NULL, '2022-04-22 11:33:34');
INSERT INTO `gen_table_column` VALUES (60, '4', 'dict_conver', '字典转换（1、开，2、关）dictzhuanhuan', 'int', 'Integer', 'dictConver', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 8, 'admin', '2022-04-22 11:28:35', NULL, '2022-04-22 11:33:34');
INSERT INTO `gen_table_column` VALUES (61, '4', 'format_clean', '格式清洗（1、开，2、关）geshiqingxi', 'int', 'Integer', 'formatClean', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 9, 'admin', '2022-04-22 11:28:35', NULL, '2022-04-22 11:33:34');
INSERT INTO `gen_table_column` VALUES (62, '4', 'encrypt_push', '加密推送（1、开，2、关）jiamituisong', 'int', 'Integer', 'encryptPush', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 10, 'admin', '2022-04-22 11:28:35', NULL, '2022-04-22 11:33:34');
INSERT INTO `gen_table_column` VALUES (63, '4', 'decrypt_push', '解密推送（1、开，2、关）jiemituisong', 'int', 'Integer', 'decryptPush', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 11, 'admin', '2022-04-22 11:28:35', NULL, '2022-04-22 11:33:34');
INSERT INTO `gen_table_column` VALUES (64, '4', 'name', '点位全称', 'varchar(255)', 'String', 'name', '0', '0', NULL, '1', '1', '1', '1', 'LIKE', 'input', '', 12, 'admin', '2022-04-22 11:28:35', NULL, '2022-04-22 11:33:34');
INSERT INTO `gen_table_column` VALUES (65, '4', 'longitude', '经度', 'decimal(65,8)', 'BigDecimal', 'longitude', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 13, 'admin', '2022-04-22 11:28:35', NULL, '2022-04-22 11:33:34');
INSERT INTO `gen_table_column` VALUES (66, '4', 'latitude', '纬度', 'decimal(65,8)', 'BigDecimal', 'latitude', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 14, 'admin', '2022-04-22 11:28:35', NULL, '2022-04-22 11:33:34');
INSERT INTO `gen_table_column` VALUES (67, '4', 'police', '派出所', 'varchar(255)', 'String', 'police', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 15, 'admin', '2022-04-22 11:28:35', NULL, '2022-04-22 11:33:34');
INSERT INTO `gen_table_column` VALUES (68, '4', 'type', '类型', 'varchar(255)', 'String', 'type', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'select', '', 16, 'admin', '2022-04-22 11:28:35', NULL, '2022-04-22 11:33:34');
INSERT INTO `gen_table_column` VALUES (69, '4', 'create_time', '创建时间', 'datetime', 'String', 'createTime', '0', '0', NULL, '1', NULL, NULL, NULL, 'EQ', 'datetime', NULL, 17, '', '2022-04-22 11:29:29', NULL, '2022-04-22 11:33:34');
INSERT INTO `gen_table_column` VALUES (70, '5', 'ID', '主键', 'varchar(255)', 'String', 'ID', '1', '0', NULL, '1', NULL, NULL, NULL, 'EQ', 'input', '', 1, 'admin', '2022-06-27 16:01:47', '', '2022-06-27 16:25:32');
INSERT INTO `gen_table_column` VALUES (71, '5', 'MotorVehicleID', NULL, 'varchar(255)', 'String', 'MotorVehicleID', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 2, 'admin', '2022-06-27 16:01:47', '', '2022-06-27 16:25:32');
INSERT INTO `gen_table_column` VALUES (72, '5', 'InfoKind', NULL, 'varchar(255)', 'String', 'InfoKind', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 3, 'admin', '2022-06-27 16:01:47', '', '2022-06-27 16:25:32');
INSERT INTO `gen_table_column` VALUES (73, '5', 'SourceID', NULL, 'varchar(255)', 'String', 'SourceID', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 4, 'admin', '2022-06-27 16:01:47', '', '2022-06-27 16:25:32');
INSERT INTO `gen_table_column` VALUES (74, '5', 'TollgateID', NULL, 'varchar(255)', 'String', 'TollgateID', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 5, 'admin', '2022-06-27 16:01:47', '', '2022-06-27 16:25:32');
INSERT INTO `gen_table_column` VALUES (75, '5', 'DeviceID', NULL, 'varchar(255)', 'String', 'DeviceID', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 6, 'admin', '2022-06-27 16:01:47', '', '2022-06-27 16:25:32');
INSERT INTO `gen_table_column` VALUES (76, '5', 'StorageUrl1', NULL, 'varchar(255)', 'String', 'StorageUrl1', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 7, 'admin', '2022-06-27 16:01:47', '', '2022-06-27 16:25:32');
INSERT INTO `gen_table_column` VALUES (77, '5', 'StorageUrl2', NULL, 'varchar(255)', 'String', 'StorageUrl2', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 8, 'admin', '2022-06-27 16:01:47', '', '2022-06-27 16:25:32');
INSERT INTO `gen_table_column` VALUES (78, '5', 'StorageUrl3', NULL, 'varchar(255)', 'String', 'StorageUrl3', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 9, 'admin', '2022-06-27 16:01:47', '', '2022-06-27 16:25:32');
INSERT INTO `gen_table_column` VALUES (79, '5', 'StorageUrl4', NULL, 'varchar(255)', 'String', 'StorageUrl4', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 10, 'admin', '2022-06-27 16:01:47', '', '2022-06-27 16:25:32');
INSERT INTO `gen_table_column` VALUES (80, '5', 'StorageUrl5', NULL, 'varchar(255)', 'String', 'StorageUrl5', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 11, 'admin', '2022-06-27 16:01:47', '', '2022-06-27 16:25:32');
INSERT INTO `gen_table_column` VALUES (81, '5', 'LeftTopX', NULL, 'varchar(255)', 'String', 'LeftTopX', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 12, 'admin', '2022-06-27 16:01:47', '', '2022-06-27 16:25:32');
INSERT INTO `gen_table_column` VALUES (82, '5', 'LeftTopY', NULL, 'varchar(255)', 'String', 'LeftTopY', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 13, 'admin', '2022-06-27 16:01:47', '', '2022-06-27 16:25:33');
INSERT INTO `gen_table_column` VALUES (83, '5', 'RightBtmX', NULL, 'varchar(255)', 'String', 'RightBtmX', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 14, 'admin', '2022-06-27 16:01:47', '', '2022-06-27 16:25:33');
INSERT INTO `gen_table_column` VALUES (84, '5', 'RightBtmY', NULL, 'varchar(255)', 'String', 'RightBtmY', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 15, 'admin', '2022-06-27 16:01:48', '', '2022-06-27 16:25:33');
INSERT INTO `gen_table_column` VALUES (85, '5', 'LaneNo', NULL, 'varchar(255)', 'String', 'LaneNo', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 16, 'admin', '2022-06-27 16:01:48', '', '2022-06-27 16:25:33');
INSERT INTO `gen_table_column` VALUES (86, '5', 'HasPlate', NULL, 'varchar(255)', 'String', 'HasPlate', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 17, 'admin', '2022-06-27 16:01:48', '', '2022-06-27 16:25:33');
INSERT INTO `gen_table_column` VALUES (87, '5', 'PlateClass', NULL, 'varchar(255)', 'String', 'PlateClass', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 18, 'admin', '2022-06-27 16:01:48', '', '2022-06-27 16:25:33');
INSERT INTO `gen_table_column` VALUES (88, '5', 'PlateColor', NULL, 'varchar(255)', 'String', 'PlateColor', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 19, 'admin', '2022-06-27 16:01:48', '', '2022-06-27 16:25:33');
INSERT INTO `gen_table_column` VALUES (89, '5', 'PlateNo', NULL, 'varchar(255)', 'String', 'PlateNo', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 20, 'admin', '2022-06-27 16:01:48', '', '2022-06-27 16:25:33');
INSERT INTO `gen_table_column` VALUES (90, '5', 'PlateNoAttach', NULL, 'varchar(255)', 'String', 'PlateNoAttach', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 21, 'admin', '2022-06-27 16:01:48', '', '2022-06-27 16:25:33');
INSERT INTO `gen_table_column` VALUES (91, '5', 'PlateDescribe', NULL, 'varchar(255)', 'String', 'PlateDescribe', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 22, 'admin', '2022-06-27 16:01:48', '', '2022-06-27 16:25:33');
INSERT INTO `gen_table_column` VALUES (92, '5', 'IsDecked', NULL, 'varchar(255)', 'String', 'IsDecked', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 23, 'admin', '2022-06-27 16:01:48', '', '2022-06-27 16:25:33');
INSERT INTO `gen_table_column` VALUES (93, '5', 'IsAltered', NULL, 'varchar(255)', 'String', 'IsAltered', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 24, 'admin', '2022-06-27 16:01:48', '', '2022-06-27 16:25:33');
INSERT INTO `gen_table_column` VALUES (94, '5', 'IsCovered', NULL, 'varchar(255)', 'String', 'IsCovered', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 25, 'admin', '2022-06-27 16:01:48', '', '2022-06-27 16:25:33');
INSERT INTO `gen_table_column` VALUES (95, '5', 'Speed', NULL, 'varchar(255)', 'String', 'Speed', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 26, 'admin', '2022-06-27 16:01:48', '', '2022-06-27 16:25:34');
INSERT INTO `gen_table_column` VALUES (96, '5', 'Direction', NULL, 'varchar(255)', 'String', 'Direction', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 27, 'admin', '2022-06-27 16:01:48', '', '2022-06-27 16:25:34');
INSERT INTO `gen_table_column` VALUES (97, '5', 'DrivingStatusCode', NULL, 'varchar(255)', 'String', 'DrivingStatusCode', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 28, 'admin', '2022-06-27 16:01:48', '', '2022-06-27 16:25:34');
INSERT INTO `gen_table_column` VALUES (98, '5', 'UsingPropertiesCode', NULL, 'varchar(255)', 'String', 'UsingPropertiesCode', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 29, 'admin', '2022-06-27 16:01:48', '', '2022-06-27 16:25:34');
INSERT INTO `gen_table_column` VALUES (99, '5', 'VehicleClass', NULL, 'varchar(255)', 'String', 'VehicleClass', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 30, 'admin', '2022-06-27 16:01:48', '', '2022-06-27 16:25:34');
INSERT INTO `gen_table_column` VALUES (100, '5', 'VehicleBrand', NULL, 'varchar(255)', 'String', 'VehicleBrand', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 31, 'admin', '2022-06-27 16:01:48', '', '2022-06-27 16:25:34');
INSERT INTO `gen_table_column` VALUES (101, '5', 'VehicleModel', NULL, 'varchar(255)', 'String', 'VehicleModel', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 32, 'admin', '2022-06-27 16:01:49', '', '2022-06-27 16:25:34');
INSERT INTO `gen_table_column` VALUES (102, '5', 'VehicleStyles', NULL, 'varchar(255)', 'String', 'VehicleStyles', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 33, 'admin', '2022-06-27 16:01:49', '', '2022-06-27 16:25:34');
INSERT INTO `gen_table_column` VALUES (103, '5', 'VehicleLength', NULL, 'varchar(255)', 'String', 'VehicleLength', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 34, 'admin', '2022-06-27 16:01:49', '', '2022-06-27 16:25:34');
INSERT INTO `gen_table_column` VALUES (104, '5', 'VehicleWidth', NULL, 'varchar(255)', 'String', 'VehicleWidth', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 35, 'admin', '2022-06-27 16:01:49', '', '2022-06-27 16:25:34');
INSERT INTO `gen_table_column` VALUES (105, '5', 'VehicleHeight', NULL, 'varchar(255)', 'String', 'VehicleHeight', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 36, 'admin', '2022-06-27 16:01:49', '', '2022-06-27 16:25:34');
INSERT INTO `gen_table_column` VALUES (106, '5', 'VehicleColor', NULL, 'varchar(255)', 'String', 'VehicleColor', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 37, 'admin', '2022-06-27 16:01:49', '', '2022-06-27 16:25:34');
INSERT INTO `gen_table_column` VALUES (107, '5', 'VehicleColorDepth', NULL, 'varchar(255)', 'String', 'VehicleColorDepth', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 38, 'admin', '2022-06-27 16:01:49', '', '2022-06-27 16:25:34');
INSERT INTO `gen_table_column` VALUES (108, '5', 'VehicleHood', NULL, 'varchar(255)', 'String', 'VehicleHood', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 39, 'admin', '2022-06-27 16:01:49', '', '2022-06-27 16:25:35');
INSERT INTO `gen_table_column` VALUES (109, '5', 'VehicleTrunk', NULL, 'varchar(255)', 'String', 'VehicleTrunk', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 40, 'admin', '2022-06-27 16:01:49', '', '2022-06-27 16:25:35');
INSERT INTO `gen_table_column` VALUES (110, '5', 'VehicleWheel', NULL, 'varchar(255)', 'String', 'VehicleWheel', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 41, 'admin', '2022-06-27 16:01:49', '', '2022-06-27 16:25:35');
INSERT INTO `gen_table_column` VALUES (111, '5', 'WheelPrintedPattern', NULL, 'varchar(255)', 'String', 'WheelPrintedPattern', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 42, 'admin', '2022-06-27 16:01:49', '', '2022-06-27 16:25:35');
INSERT INTO `gen_table_column` VALUES (112, '5', 'VehicleWindow', NULL, 'varchar(255)', 'String', 'VehicleWindow', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 43, 'admin', '2022-06-27 16:01:49', '', '2022-06-27 16:25:35');
INSERT INTO `gen_table_column` VALUES (113, '5', 'VehicleRoof', NULL, 'varchar(255)', 'String', 'VehicleRoof', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 44, 'admin', '2022-06-27 16:01:49', '', '2022-06-27 16:25:35');
INSERT INTO `gen_table_column` VALUES (114, '5', 'VehicleDoor', NULL, 'varchar(255)', 'String', 'VehicleDoor', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 45, 'admin', '2022-06-27 16:01:49', '', '2022-06-27 16:25:35');
INSERT INTO `gen_table_column` VALUES (115, '5', 'SideOfVehicle', NULL, 'varchar(255)', 'String', 'SideOfVehicle', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 46, 'admin', '2022-06-27 16:01:49', '', '2022-06-27 16:25:35');
INSERT INTO `gen_table_column` VALUES (116, '5', 'CarOfVehicle', NULL, 'varchar(255)', 'String', 'CarOfVehicle', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 47, 'admin', '2022-06-27 16:01:49', '', '2022-06-27 16:25:35');
INSERT INTO `gen_table_column` VALUES (117, '5', 'RearviewMirror', NULL, 'varchar(255)', 'String', 'RearviewMirror', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 48, 'admin', '2022-06-27 16:01:50', '', '2022-06-27 16:25:35');
INSERT INTO `gen_table_column` VALUES (118, '5', 'VehicleChassis', NULL, 'varchar(255)', 'String', 'VehicleChassis', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 49, 'admin', '2022-06-27 16:01:50', '', '2022-06-27 16:25:35');
INSERT INTO `gen_table_column` VALUES (119, '5', 'VehicleShielding', NULL, 'varchar(255)', 'String', 'VehicleShielding', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 50, 'admin', '2022-06-27 16:01:50', '', '2022-06-27 16:25:35');
INSERT INTO `gen_table_column` VALUES (120, '5', 'FilmColor', NULL, 'varchar(255)', 'String', 'FilmColor', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 51, 'admin', '2022-06-27 16:01:50', '', '2022-06-27 16:25:35');
INSERT INTO `gen_table_column` VALUES (121, '5', 'IsModified', NULL, 'varchar(255)', 'String', 'IsModified', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 52, 'admin', '2022-06-27 16:01:50', '', '2022-06-27 16:25:36');
INSERT INTO `gen_table_column` VALUES (122, '5', 'HitMarkInfo', NULL, 'varchar(255)', 'String', 'HitMarkInfo', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 53, 'admin', '2022-06-27 16:01:50', '', '2022-06-27 16:25:36');
INSERT INTO `gen_table_column` VALUES (123, '5', 'VehicleBodyDesc', NULL, 'varchar(255)', 'String', 'VehicleBodyDesc', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 54, 'admin', '2022-06-27 16:01:50', '', '2022-06-27 16:25:36');
INSERT INTO `gen_table_column` VALUES (124, '5', 'VehicleFrontItem', NULL, 'varchar(255)', 'String', 'VehicleFrontItem', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 55, 'admin', '2022-06-27 16:01:50', '', '2022-06-27 16:25:36');
INSERT INTO `gen_table_column` VALUES (125, '5', 'DescOfFrontItem', NULL, 'varchar(255)', 'String', 'DescOfFrontItem', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 56, 'admin', '2022-06-27 16:01:50', '', '2022-06-27 16:25:36');
INSERT INTO `gen_table_column` VALUES (126, '5', 'VehicleRearItem', NULL, 'varchar(255)', 'String', 'VehicleRearItem', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 57, 'admin', '2022-06-27 16:01:50', '', '2022-06-27 16:25:36');
INSERT INTO `gen_table_column` VALUES (127, '5', 'DescOfRearItem', NULL, 'varchar(255)', 'String', 'DescOfRearItem', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 58, 'admin', '2022-06-27 16:01:50', '', '2022-06-27 16:25:36');
INSERT INTO `gen_table_column` VALUES (128, '5', 'NumOfPassenger', NULL, 'varchar(255)', 'String', 'NumOfPassenger', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 59, 'admin', '2022-06-27 16:01:50', '', '2022-06-27 16:25:36');
INSERT INTO `gen_table_column` VALUES (129, '5', 'PassTime', NULL, 'datetime', 'String', 'PassTime', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'datetime', '', 60, 'admin', '2022-06-27 16:01:50', '', '2022-06-27 16:25:36');
INSERT INTO `gen_table_column` VALUES (130, '5', 'NameOfPassedRoad', NULL, 'varchar(255)', 'String', 'NameOfPassedRoad', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 61, 'admin', '2022-06-27 16:01:50', '', '2022-06-27 16:25:36');
INSERT INTO `gen_table_column` VALUES (131, '5', 'IsSuspicious', NULL, 'varchar(255)', 'String', 'IsSuspicious', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 62, 'admin', '2022-06-27 16:01:50', '', '2022-06-27 16:25:36');
INSERT INTO `gen_table_column` VALUES (132, '5', 'PlateReliability', NULL, 'varchar(255)', 'String', 'PlateReliability', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 63, 'admin', '2022-06-27 16:01:51', '', '2022-06-27 16:25:36');
INSERT INTO `gen_table_column` VALUES (133, '5', 'PlateCharReliability', NULL, 'varchar(255)', 'String', 'PlateCharReliability', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 64, 'admin', '2022-06-27 16:01:51', '', '2022-06-27 16:25:37');
INSERT INTO `gen_table_column` VALUES (134, '5', 'BrandReliability', NULL, 'varchar(255)', 'String', 'BrandReliability', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 65, 'admin', '2022-06-27 16:01:51', '', '2022-06-27 16:25:37');
INSERT INTO `gen_table_column` VALUES (135, '5', 'AppearTime', NULL, 'datetime', 'String', 'AppearTime', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'datetime', '', 66, 'admin', '2022-06-27 16:01:51', '', '2022-06-27 16:25:37');
INSERT INTO `gen_table_column` VALUES (136, '5', 'Calling', NULL, 'varchar(255)', 'String', 'Calling', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 67, 'admin', '2022-06-27 16:01:51', '', '2022-06-27 16:25:37');
INSERT INTO `gen_table_column` VALUES (137, '5', 'DisappearTime', NULL, 'datetime', 'String', 'DisappearTime', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'datetime', '', 68, 'admin', '2022-06-27 16:01:51', '', '2022-06-27 16:25:37');
INSERT INTO `gen_table_column` VALUES (138, '5', 'MarkTime', NULL, 'datetime', 'String', 'MarkTime', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'datetime', '', 69, 'admin', '2022-06-27 16:01:51', '', '2022-06-27 16:25:37');
INSERT INTO `gen_table_column` VALUES (139, '5', 'Sunvisor', NULL, 'varchar(255)', 'String', 'Sunvisor', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 70, 'admin', '2022-06-27 16:01:51', '', '2022-06-27 16:25:37');
INSERT INTO `gen_table_column` VALUES (140, '5', 'SafetyBelt', NULL, 'varchar(255)', 'String', 'SafetyBelt', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 71, 'admin', '2022-06-27 16:01:51', '', '2022-06-27 16:25:37');
INSERT INTO `gen_table_column` VALUES (141, '5', 'CreateTime', NULL, 'datetime', 'String', 'CreateTime', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'datetime', '', 72, 'admin', '2022-06-27 16:01:51', '', '2022-06-27 16:25:37');
INSERT INTO `gen_table_column` VALUES (142, '6', 'ID', '编号', 'varchar(255)', 'String', 'ID', '1', '0', NULL, '1', NULL, NULL, NULL, 'EQ', 'input', NULL, 1, 'admin', '2022-06-27 16:01:51', NULL, '2022-06-27 16:55:33');
INSERT INTO `gen_table_column` VALUES (144, '6', 'ImageID', '图像标识', 'varchar(255)', 'String', 'ImageID', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 3, 'admin', '2022-06-27 16:01:51', NULL, '2022-06-27 16:55:33');
INSERT INTO `gen_table_column` VALUES (145, '6', 'EventSort', '事件分类', 'varchar(255)', 'String', 'EventSort', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 4, 'admin', '2022-06-27 16:01:51', NULL, '2022-06-27 16:55:33');
INSERT INTO `gen_table_column` VALUES (146, '6', 'DeviceID', '设备编码', 'varchar(255)', 'String', 'DeviceID', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 5, 'admin', '2022-06-27 16:01:51', NULL, '2022-06-27 16:55:33');
INSERT INTO `gen_table_column` VALUES (147, '6', 'StoragePath', '存储路径', 'varchar(255)', 'String', 'StoragePath', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 6, 'admin', '2022-06-27 16:01:52', NULL, '2022-06-27 16:55:33');
INSERT INTO `gen_table_column` VALUES (148, '6', 'Type', '不清楚字段含义', 'varchar(255)', 'String', 'Type', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'select', '', 7, 'admin', '2022-06-27 16:01:52', NULL, '2022-06-27 16:55:33');
INSERT INTO `gen_table_column` VALUES (149, '6', 'FileFormat', '图像文件格式', 'varchar(255)', 'String', 'FileFormat', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 8, 'admin', '2022-06-27 16:01:52', NULL, '2022-06-27 16:55:33');
INSERT INTO `gen_table_column` VALUES (150, '6', 'ShotTime', '拍摄时间', 'datetime', 'String', 'ShotTime', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'datetime', '', 9, 'admin', '2022-06-27 16:01:52', NULL, '2022-06-27 16:55:33');
INSERT INTO `gen_table_column` VALUES (151, '6', 'Width', '宽度', 'varchar(255)', 'String', 'Width', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 10, 'admin', '2022-06-27 16:01:52', NULL, '2022-06-27 16:55:33');
INSERT INTO `gen_table_column` VALUES (152, '6', 'Height', '高度', 'varchar(255)', 'String', 'Height', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 11, 'admin', '2022-06-27 16:01:52', NULL, '2022-06-27 16:55:34');
INSERT INTO `gen_table_column` VALUES (153, '6', 'ImageUrl', 'minio上传后的图片地址', 'varchar(255)', 'String', 'ImageUrl', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 12, 'admin', '2022-06-27 16:01:52', NULL, '2022-06-27 16:55:34');
INSERT INTO `gen_table_column` VALUES (155, '6', 'create_time', '上传时间', 'datetime', 'Date', 'createTime', '0', '0', NULL, '1', NULL, NULL, NULL, 'EQ', 'datetime', NULL, 13, '', '2022-06-27 16:49:45', NULL, '2022-06-27 16:55:34');
INSERT INTO `gen_table_column` VALUES (156, '6', 'MotorVehicleID', '车辆标识符', 'varchar(255)', 'String', 'MotorVehicleID', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 2, '', '2022-06-27 16:54:22', '', '2022-06-27 16:55:33');
INSERT INTO `gen_table_column` VALUES (157, '7', 'ID', '主键', 'varchar(30)', 'String', 'ID', '1', '0', NULL, '1', NULL, NULL, NULL, 'EQ', 'input', '', 1, 'admin', '2022-06-29 10:47:08', '', '2022-06-29 11:00:44');
INSERT INTO `gen_table_column` VALUES (158, '7', 'PersonID', '人员标识', 'varchar(50)', 'String', 'PersonID', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 2, 'admin', '2022-06-29 10:47:08', '', '2022-06-29 11:00:44');
INSERT INTO `gen_table_column` VALUES (159, '7', 'InfoKind', '信息分类', 'varchar(50)', 'String', 'InfoKind', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 3, 'admin', '2022-06-29 10:47:08', '', '2022-06-29 11:00:44');
INSERT INTO `gen_table_column` VALUES (160, '7', 'SourceID', '来源标识', 'varchar(255)', 'String', 'SourceID', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 4, 'admin', '2022-06-29 10:47:08', '', '2022-06-29 11:00:44');
INSERT INTO `gen_table_column` VALUES (161, '7', 'DeviceID', '设备编码', 'varchar(50)', 'String', 'DeviceID', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 5, 'admin', '2022-06-29 10:47:08', '', '2022-06-29 11:00:44');
INSERT INTO `gen_table_column` VALUES (162, '7', 'LeftTopX', '左上角X坐标', 'int', 'Integer', 'LeftTopX', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 6, 'admin', '2022-06-29 10:47:08', '', '2022-06-29 11:00:44');
INSERT INTO `gen_table_column` VALUES (163, '7', 'LeftTopY', '左上角Y坐标', 'int', 'Integer', 'LeftTopY', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 7, 'admin', '2022-06-29 10:47:08', '', '2022-06-29 11:00:44');
INSERT INTO `gen_table_column` VALUES (164, '7', 'RightBtmX', '右下角X坐标', 'int', 'Integer', 'RightBtmX', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 8, 'admin', '2022-06-29 10:47:09', '', '2022-06-29 11:00:44');
INSERT INTO `gen_table_column` VALUES (165, '7', 'RightBtmY', '右下角Y坐标', 'int', 'Integer', 'RightBtmY', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 9, 'admin', '2022-06-29 10:47:09', '', '2022-06-29 11:00:44');
INSERT INTO `gen_table_column` VALUES (166, '7', 'PersonAppearTime', '人体出现时间', 'datetime', 'Date', 'PersonAppearTime', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'datetime', '', 10, 'admin', '2022-06-29 10:47:09', '', '2022-06-29 11:00:44');
INSERT INTO `gen_table_column` VALUES (167, '7', 'LocationMarkTime', '位置标记时间', 'datetime', 'Date', 'LocationMarkTime', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'datetime', '', 11, 'admin', '2022-06-29 10:47:09', '', '2022-06-29 11:00:44');
INSERT INTO `gen_table_column` VALUES (168, '7', 'PersonDisAppearTime', '人员消失时间', 'datetime', 'Date', 'PersonDisAppearTime', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'datetime', '', 12, 'admin', '2022-06-29 10:47:09', '', '2022-06-29 11:00:44');
INSERT INTO `gen_table_column` VALUES (169, '7', 'IDType', '证件种类', 'varchar(255)', 'String', 'IDType', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'select', '', 13, 'admin', '2022-06-29 10:47:09', '', '2022-06-29 11:00:45');
INSERT INTO `gen_table_column` VALUES (170, '7', 'IDNumber', '证件号码', 'varchar(255)', 'String', 'IDNumber', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 14, 'admin', '2022-06-29 10:47:09', '', '2022-06-29 11:00:45');
INSERT INTO `gen_table_column` VALUES (171, '7', 'Name', '姓名', 'varchar(255)', 'String', 'Name', '0', '0', NULL, '1', '1', '1', '1', 'LIKE', 'input', '', 15, 'admin', '2022-06-29 10:47:09', '', '2022-06-29 11:00:45');
INSERT INTO `gen_table_column` VALUES (172, '7', 'UsedName', '曾用名', 'varchar(255)', 'String', 'UsedName', '0', '0', NULL, '1', '1', '1', '1', 'LIKE', 'input', '', 16, 'admin', '2022-06-29 10:47:09', '', '2022-06-29 11:00:45');
INSERT INTO `gen_table_column` VALUES (173, '7', 'Alias', '绰号', 'varchar(255)', 'String', 'Alias', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 17, 'admin', '2022-06-29 10:47:09', '', '2022-06-29 11:00:45');
INSERT INTO `gen_table_column` VALUES (174, '7', 'GenderCode', '性别代码', 'varchar(255)', 'String', 'GenderCode', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 18, 'admin', '2022-06-29 10:47:09', '', '2022-06-29 11:00:45');
INSERT INTO `gen_table_column` VALUES (175, '7', 'AgeUpLimit', '年龄上限', 'int', 'Integer', 'AgeUpLimit', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 19, 'admin', '2022-06-29 10:47:09', '', '2022-06-29 11:00:45');
INSERT INTO `gen_table_column` VALUES (176, '7', 'AgeLowerLimit', '年龄下限', 'int', 'Integer', 'AgeLowerLimit', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 20, 'admin', '2022-06-29 10:47:10', '', '2022-06-29 11:00:45');
INSERT INTO `gen_table_column` VALUES (177, '7', 'EthicCode', '民族代码', 'varchar(255)', 'String', 'EthicCode', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 21, 'admin', '2022-06-29 10:47:10', '', '2022-06-29 11:00:45');
INSERT INTO `gen_table_column` VALUES (178, '7', 'NationalityCode', '国籍代码', 'varchar(255)', 'String', 'NationalityCode', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 22, 'admin', '2022-06-29 10:47:10', '', '2022-06-29 11:00:45');
INSERT INTO `gen_table_column` VALUES (179, '7', 'NativeCityCode', '籍贯省市县代码', 'varchar(255)', 'String', 'NativeCityCode', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 23, 'admin', '2022-06-29 10:47:10', '', '2022-06-29 11:00:45');
INSERT INTO `gen_table_column` VALUES (180, '7', 'ResidenceAdminDivision', '居住地行政区划', 'varchar(255)', 'String', 'ResidenceAdminDivision', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 24, 'admin', '2022-06-29 10:47:10', '', '2022-06-29 11:00:45');
INSERT INTO `gen_table_column` VALUES (181, '7', 'ChineseAccentCode', '汉语口音代码', 'varchar(255)', 'String', 'ChineseAccentCode', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 25, 'admin', '2022-06-29 10:47:10', '', '2022-06-29 11:00:45');
INSERT INTO `gen_table_column` VALUES (182, '7', 'PersonOrg', '单位名称', 'varchar(255)', 'String', 'PersonOrg', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 26, 'admin', '2022-06-29 10:47:10', '', '2022-06-29 11:00:45');
INSERT INTO `gen_table_column` VALUES (183, '7', 'JobCategory', '职业类别代码', 'varchar(255)', 'String', 'JobCategory', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 27, 'admin', '2022-06-29 10:47:10', '', '2022-06-29 11:00:45');
INSERT INTO `gen_table_column` VALUES (184, '7', 'AccompanyNumber', '同行人员', 'int', 'Integer', 'AccompanyNumber', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 28, 'admin', '2022-06-29 10:47:10', '', '2022-06-29 11:00:45');
INSERT INTO `gen_table_column` VALUES (185, '7', 'HeightUpLimit', '身高上限', 'int', 'Integer', 'HeightUpLimit', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 29, 'admin', '2022-06-29 10:47:10', '', '2022-06-29 11:00:46');
INSERT INTO `gen_table_column` VALUES (186, '7', 'HeightLowerLimit', '身高下限', 'int', 'Integer', 'HeightLowerLimit', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 30, 'admin', '2022-06-29 10:47:10', '', '2022-06-29 11:00:46');
INSERT INTO `gen_table_column` VALUES (187, '7', 'BodyType', '体型', 'varchar(255)', 'String', 'BodyType', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'select', '', 31, 'admin', '2022-06-29 10:47:10', '', '2022-06-29 11:00:46');
INSERT INTO `gen_table_column` VALUES (188, '7', 'SkinColor', '肤色', 'varchar(255)', 'String', 'SkinColor', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 32, 'admin', '2022-06-29 10:47:10', '', '2022-06-29 11:00:46');
INSERT INTO `gen_table_column` VALUES (189, '7', 'HairStyle', '发型', 'varchar(255)', 'String', 'HairStyle', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 33, 'admin', '2022-06-29 10:47:10', '', '2022-06-29 11:00:46');
INSERT INTO `gen_table_column` VALUES (190, '7', 'HairColor', '发色', 'varchar(255)', 'String', 'HairColor', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 34, 'admin', '2022-06-29 10:47:11', '', '2022-06-29 11:00:46');
INSERT INTO `gen_table_column` VALUES (191, '7', 'Gesture', '姿态', 'varchar(255)', 'String', 'Gesture', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 35, 'admin', '2022-06-29 10:47:11', '', '2022-06-29 11:00:46');
INSERT INTO `gen_table_column` VALUES (192, '7', 'Status', '状态', 'varchar(255)', 'String', 'Status', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'radio', '', 36, 'admin', '2022-06-29 10:47:11', '', '2022-06-29 11:00:46');
INSERT INTO `gen_table_column` VALUES (193, '7', 'FaceStyle', '脸型', 'varchar(255)', 'String', 'FaceStyle', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 37, 'admin', '2022-06-29 10:47:11', '', '2022-06-29 11:00:46');
INSERT INTO `gen_table_column` VALUES (194, '7', 'FacialFeature', '脸部特征', 'varchar(255)', 'String', 'FacialFeature', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 38, 'admin', '2022-06-29 10:47:11', '', '2022-06-29 11:00:46');
INSERT INTO `gen_table_column` VALUES (195, '7', 'PhysicalFeature', '体貌特征', 'varchar(255)', 'String', 'PhysicalFeature', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 39, 'admin', '2022-06-29 10:47:11', '', '2022-06-29 11:00:46');
INSERT INTO `gen_table_column` VALUES (196, '7', 'BodyFeature', '体表特征', 'varchar(255)', 'String', 'BodyFeature', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 40, 'admin', '2022-06-29 10:47:11', '', '2022-06-29 11:00:46');
INSERT INTO `gen_table_column` VALUES (197, '7', 'HabitualMovement', '习惯动作', 'varchar(255)', 'String', 'HabitualMovement', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 41, 'admin', '2022-06-29 10:47:11', '', '2022-06-29 11:00:46');
INSERT INTO `gen_table_column` VALUES (198, '7', 'Behavior', '行为', 'varchar(255)', 'String', 'Behavior', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 42, 'admin', '2022-06-29 10:47:11', '', '2022-06-29 11:00:46');
INSERT INTO `gen_table_column` VALUES (199, '7', 'BehaviorDescription', '行为描述', 'varchar(255)', 'String', 'BehaviorDescription', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 43, 'admin', '2022-06-29 10:47:11', '', '2022-06-29 11:00:47');
INSERT INTO `gen_table_column` VALUES (200, '7', 'Appendant', '附属物', 'varchar(255)', 'String', 'Appendant', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 44, 'admin', '2022-06-29 10:47:11', '', '2022-06-29 11:00:47');
INSERT INTO `gen_table_column` VALUES (201, '7', 'AppendantDescription', '附属物描述', 'varchar(255)', 'String', 'AppendantDescription', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 45, 'admin', '2022-06-29 10:47:11', '', '2022-06-29 11:00:47');
INSERT INTO `gen_table_column` VALUES (202, '7', 'UmbrellaColor', '伞颜色', 'varchar(255)', 'String', 'UmbrellaColor', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 46, 'admin', '2022-06-29 10:47:11', '', '2022-06-29 11:00:47');
INSERT INTO `gen_table_column` VALUES (203, '7', 'RespiratorColor', '口罩颜色', 'varchar(255)', 'String', 'RespiratorColor', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 47, 'admin', '2022-06-29 10:47:11', '', '2022-06-29 11:00:47');
INSERT INTO `gen_table_column` VALUES (204, '7', 'CapStyle', '帽子款式', 'varchar(255)', 'String', 'CapStyle', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 48, 'admin', '2022-06-29 10:47:11', '', '2022-06-29 11:00:47');
INSERT INTO `gen_table_column` VALUES (205, '7', 'CapColor', '帽子颜色', 'varchar(255)', 'String', 'CapColor', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 49, 'admin', '2022-06-29 10:47:12', '', '2022-06-29 11:00:47');
INSERT INTO `gen_table_column` VALUES (206, '7', 'GlassStyle', '眼镜款式', 'varchar(255)', 'String', 'GlassStyle', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 50, 'admin', '2022-06-29 10:47:12', '', '2022-06-29 11:00:47');
INSERT INTO `gen_table_column` VALUES (207, '7', 'GlassColor', '眼镜颜色', 'varchar(255)', 'String', 'GlassColor', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 51, 'admin', '2022-06-29 10:47:12', '', '2022-06-29 11:00:47');
INSERT INTO `gen_table_column` VALUES (208, '7', 'ScarfColor', '围巾颜色', 'varchar(255)', 'String', 'ScarfColor', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 52, 'admin', '2022-06-29 10:47:12', '', '2022-06-29 11:00:47');
INSERT INTO `gen_table_column` VALUES (209, '7', 'BagStyle', '包款式', 'varchar(255)', 'String', 'BagStyle', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 53, 'admin', '2022-06-29 10:47:12', '', '2022-06-29 11:00:47');
INSERT INTO `gen_table_column` VALUES (210, '7', 'BagColor', '包颜色', 'varchar(255)', 'String', 'BagColor', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 54, 'admin', '2022-06-29 10:47:12', '', '2022-06-29 11:00:47');
INSERT INTO `gen_table_column` VALUES (211, '7', 'CoatStyle', '上衣款式', 'varchar(255)', 'String', 'CoatStyle', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 55, 'admin', '2022-06-29 10:47:12', '', '2022-06-29 11:00:47');
INSERT INTO `gen_table_column` VALUES (212, '7', 'CoatLength', '上衣长度', 'varchar(255)', 'String', 'CoatLength', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 56, 'admin', '2022-06-29 10:47:12', '', '2022-06-29 11:00:47');
INSERT INTO `gen_table_column` VALUES (213, '7', 'CoatColor', '上衣颜色', 'varchar(255)', 'String', 'CoatColor', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 57, 'admin', '2022-06-29 10:47:12', '', '2022-06-29 11:00:47');
INSERT INTO `gen_table_column` VALUES (214, '7', 'TrousersStyle', '裤子款式', 'varchar(255)', 'String', 'TrousersStyle', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 58, 'admin', '2022-06-29 10:47:12', '', '2022-06-29 11:00:48');
INSERT INTO `gen_table_column` VALUES (215, '7', 'TrousersColor', '裤子颜色', 'varchar(255)', 'String', 'TrousersColor', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 59, 'admin', '2022-06-29 10:47:12', '', '2022-06-29 11:00:48');
INSERT INTO `gen_table_column` VALUES (216, '7', 'TrousersLen', '裤子长度', 'varchar(255)', 'String', 'TrousersLen', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 60, 'admin', '2022-06-29 10:47:12', '', '2022-06-29 11:00:48');
INSERT INTO `gen_table_column` VALUES (217, '7', 'ShoesStyle', '鞋子款式', 'varchar(255)', 'String', 'ShoesStyle', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 61, 'admin', '2022-06-29 10:47:12', '', '2022-06-29 11:00:48');
INSERT INTO `gen_table_column` VALUES (218, '7', 'ShoesColor', '鞋子颜色', 'varchar(255)', 'String', 'ShoesColor', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 62, 'admin', '2022-06-29 10:47:13', '', '2022-06-29 11:00:48');
INSERT INTO `gen_table_column` VALUES (219, '7', 'IsDriver', '是否驾驶员', 'int', 'Integer', 'IsDriver', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 63, 'admin', '2022-06-29 10:47:13', '', '2022-06-29 11:00:48');
INSERT INTO `gen_table_column` VALUES (220, '7', 'IsForeigner', '是否涉外人员', 'int', 'Integer', 'IsForeigner', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 64, 'admin', '2022-06-29 10:47:13', '', '2022-06-29 11:00:48');
INSERT INTO `gen_table_column` VALUES (221, '7', 'PassportType', '护照证件种类', 'varchar(255)', 'String', 'PassportType', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'select', '', 65, 'admin', '2022-06-29 10:47:13', '', '2022-06-29 11:00:48');
INSERT INTO `gen_table_column` VALUES (222, '7', 'ImmigrantTypeCode', '出入境人员类别代码', 'varchar(255)', 'String', 'ImmigrantTypeCode', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 66, 'admin', '2022-06-29 10:47:13', '', '2022-06-29 11:00:48');
INSERT INTO `gen_table_column` VALUES (223, '7', 'IsSuspectedTerrorist', '是否涉恐人员', 'int', 'Integer', 'IsSuspectedTerrorist', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 67, 'admin', '2022-06-29 10:47:13', '', '2022-06-29 11:00:48');
INSERT INTO `gen_table_column` VALUES (224, '7', 'SuspectedTerroristNumber', '涉恐人员编号', 'varchar(255)', 'String', 'SuspectedTerroristNumber', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 68, 'admin', '2022-06-29 10:47:13', '', '2022-06-29 11:00:48');
INSERT INTO `gen_table_column` VALUES (225, '7', 'IsCriminalInvolved', '是否涉案人员', 'int', 'Integer', 'IsCriminalInvolved', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 69, 'admin', '2022-06-29 10:47:13', '', '2022-06-29 11:00:48');
INSERT INTO `gen_table_column` VALUES (226, '7', 'CriminalInvolvedSpecilisationCode', '涉案人员专长代码', 'varchar(255)', 'String', 'CriminalInvolvedSpecilisationCode', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 70, 'admin', '2022-06-29 10:47:13', '', '2022-06-29 11:00:48');
INSERT INTO `gen_table_column` VALUES (227, '7', 'BodySpeciallMark', '体表特殊标记', 'varchar(255)', 'String', 'BodySpeciallMark', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 71, 'admin', '2022-06-29 10:47:13', '', '2022-06-29 11:00:48');
INSERT INTO `gen_table_column` VALUES (228, '7', 'CrimeMethod', '作案手段', 'varchar(255)', 'String', 'CrimeMethod', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 72, 'admin', '2022-06-29 10:47:13', '', '2022-06-29 11:00:48');
INSERT INTO `gen_table_column` VALUES (229, '7', 'CrimeCharacterCode', '作案特点代码', 'varchar(255)', 'String', 'CrimeCharacterCode', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 73, 'admin', '2022-06-29 10:47:13', '', '2022-06-29 11:00:48');
INSERT INTO `gen_table_column` VALUES (230, '7', 'EscapedCriminalNumber', '在逃人员编号', 'varchar(255)', 'String', 'EscapedCriminalNumber', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 74, 'admin', '2022-06-29 10:47:13', '', '2022-06-29 11:00:48');
INSERT INTO `gen_table_column` VALUES (231, '7', 'IsDetainees', '是否在押人员', 'int', 'Integer', 'IsDetainees', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 75, 'admin', '2022-06-29 10:47:14', '', '2022-06-29 11:00:49');
INSERT INTO `gen_table_column` VALUES (232, '7', 'DetentionHouseCode', '看守所编码', 'varchar(255)', 'String', 'DetentionHouseCode', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 76, 'admin', '2022-06-29 10:47:14', '', '2022-06-29 11:00:49');
INSERT INTO `gen_table_column` VALUES (233, '7', 'DetaineesIdentity', '在押人员身份', 'varchar(255)', 'String', 'DetaineesIdentity', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 77, 'admin', '2022-06-29 10:47:14', '', '2022-06-29 11:00:49');
INSERT INTO `gen_table_column` VALUES (234, '7', 'DetaineesSpecialIdentity', '在押人员特殊身份', 'varchar(255)', 'String', 'DetaineesSpecialIdentity', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 78, 'admin', '2022-06-29 10:47:14', '', '2022-06-29 11:00:49');
INSERT INTO `gen_table_column` VALUES (235, '7', 'MemberTypeCode', '成员类型代码', 'varchar(255)', 'String', 'MemberTypeCode', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 79, 'admin', '2022-06-29 10:47:14', '', '2022-06-29 11:00:49');
INSERT INTO `gen_table_column` VALUES (236, '7', 'IsVictim', '是否被害人', 'int', 'Integer', 'IsVictim', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 80, 'admin', '2022-06-29 10:47:14', '', '2022-06-29 11:00:49');
INSERT INTO `gen_table_column` VALUES (237, '7', 'VictimType', '被害人种类', 'varchar(255)', 'String', 'VictimType', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'select', '', 81, 'admin', '2022-06-29 10:47:14', '', '2022-06-29 11:00:49');
INSERT INTO `gen_table_column` VALUES (238, '7', 'InjuredDegree', '受伤害程度', 'varchar(255)', 'String', 'InjuredDegree', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 82, 'admin', '2022-06-29 10:47:14', '', '2022-06-29 11:00:50');
INSERT INTO `gen_table_column` VALUES (239, '7', 'CorpseConditionCode', '尸体状况代码', 'varchar(255)', 'String', 'CorpseConditionCode', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 83, 'admin', '2022-06-29 10:47:14', '', '2022-06-29 11:00:50');
INSERT INTO `gen_table_column` VALUES (240, '7', 'IsSuspiciousPerson', '是否可疑人', 'int', 'Integer', 'IsSuspiciousPerson', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 84, 'admin', '2022-06-29 10:47:14', '', '2022-06-29 11:00:50');
INSERT INTO `gen_table_column` VALUES (241, '7', 'create_time', '上传时间', 'datetime', 'Date', 'createTime', '0', '0', NULL, '1', NULL, NULL, NULL, 'EQ', 'datetime', '', 85, 'admin', '2022-06-29 10:47:14', '', '2022-06-29 11:00:50');
INSERT INTO `gen_table_column` VALUES (242, '8', 'ID', '主键', 'varchar(255)', 'String', 'ID', '1', '0', NULL, '1', NULL, NULL, NULL, 'EQ', 'input', '', 1, 'admin', '2022-06-29 10:47:14', '', '2022-06-29 10:49:16');
INSERT INTO `gen_table_column` VALUES (243, '8', 'PersonID', '人体标识', 'varchar(255)', 'String', 'PersonID', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 2, 'admin', '2022-06-29 10:47:14', '', '2022-06-29 10:49:16');
INSERT INTO `gen_table_column` VALUES (244, '8', 'ImageUrl', '图片地址', 'varchar(255)', 'String', 'ImageUrl', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 3, 'admin', '2022-06-29 10:47:15', '', '2022-06-29 10:49:16');
INSERT INTO `gen_table_column` VALUES (245, '8', 'ImageID', '图片标识', 'varchar(255)', 'String', 'ImageID', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 4, 'admin', '2022-06-29 10:47:15', '', '2022-06-29 10:49:17');
INSERT INTO `gen_table_column` VALUES (246, '8', 'EventSort', NULL, 'varchar(255)', 'String', 'EventSort', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 5, 'admin', '2022-06-29 10:47:15', '', '2022-06-29 10:49:17');
INSERT INTO `gen_table_column` VALUES (247, '8', 'DeviceID', '设备编号', 'varchar(255)', 'String', 'DeviceID', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 6, 'admin', '2022-06-29 10:47:15', '', '2022-06-29 10:49:17');
INSERT INTO `gen_table_column` VALUES (248, '8', 'StoragePath', NULL, 'varchar(255)', 'String', 'StoragePath', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 7, 'admin', '2022-06-29 10:47:15', '', '2022-06-29 10:49:17');
INSERT INTO `gen_table_column` VALUES (249, '8', 'Type', '未知类型', 'varchar(255)', 'String', 'Type', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'select', '', 8, 'admin', '2022-06-29 10:47:15', '', '2022-06-29 10:49:17');
INSERT INTO `gen_table_column` VALUES (250, '8', 'FileFormat', '文件格式', 'varchar(255)', 'String', 'FileFormat', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 9, 'admin', '2022-06-29 10:47:15', '', '2022-06-29 10:49:17');
INSERT INTO `gen_table_column` VALUES (251, '8', 'ShotTime', NULL, 'datetime', 'String', 'ShotTime', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'datetime', '', 10, 'admin', '2022-06-29 10:47:15', '', '2022-06-29 10:49:17');
INSERT INTO `gen_table_column` VALUES (252, '8', 'Width', '宽度', 'varchar(255)', 'String', 'Width', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 11, 'admin', '2022-06-29 10:47:15', '', '2022-06-29 10:49:17');
INSERT INTO `gen_table_column` VALUES (253, '8', 'Height', '高度', 'varchar(255)', 'String', 'Height', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 12, 'admin', '2022-06-29 10:47:15', '', '2022-06-29 10:49:17');
INSERT INTO `gen_table_column` VALUES (254, '8', 'create_time', '上传时间', 'datetime', 'Date', 'createTime', '0', '0', NULL, '1', NULL, NULL, NULL, 'EQ', 'datetime', '', 13, 'admin', '2022-06-29 10:47:15', '', '2022-06-29 10:49:17');

-- ----------------------------
-- Table structure for sys_config
-- ----------------------------
DROP TABLE IF EXISTS `sys_config`;
CREATE TABLE `sys_config`  (
  `config_id` int(0) NOT NULL AUTO_INCREMENT COMMENT '参数主键',
  `config_name` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '' COMMENT '参数名称',
  `config_key` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '' COMMENT '参数键名',
  `config_value` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '' COMMENT '参数键值',
  `config_type` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT 'N' COMMENT '系统内置（Y是 N否）',
  `create_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`config_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 100 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci COMMENT = '参数配置表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of sys_config
-- ----------------------------
INSERT INTO `sys_config` VALUES (1, '主框架页-默认皮肤样式名称', 'sys.index.skinName', 'skin-blue', 'Y', 'admin', '2022-04-15 10:31:13', '', NULL, '蓝色 skin-blue、绿色 skin-green、紫色 skin-purple、红色 skin-red、黄色 skin-yellow');
INSERT INTO `sys_config` VALUES (2, '用户管理-账号初始密码', 'sys.user.initPassword', '123456', 'Y', 'admin', '2022-04-15 10:31:13', '', NULL, '初始化密码 123456');
INSERT INTO `sys_config` VALUES (3, '主框架页-侧边栏主题', 'sys.index.sideTheme', 'theme-dark', 'Y', 'admin', '2022-04-15 10:31:13', '', NULL, '深色主题theme-dark，浅色主题theme-light');
INSERT INTO `sys_config` VALUES (4, '账号自助-验证码开关', 'sys.account.captchaOnOff', 'false', 'Y', 'admin', '2022-04-15 10:31:13', 'admin', '2022-04-15 10:45:06', '是否开启验证码功能（true开启，false关闭）');
INSERT INTO `sys_config` VALUES (5, '账号自助-是否开启用户注册功能', 'sys.account.registerUser', 'false', 'Y', 'admin', '2022-04-15 10:31:13', '', NULL, '是否开启注册用户功能（true开启，false关闭）');

-- ----------------------------
-- Table structure for sys_dept
-- ----------------------------
DROP TABLE IF EXISTS `sys_dept`;
CREATE TABLE `sys_dept`  (
  `dept_id` bigint(0) NOT NULL AUTO_INCREMENT COMMENT '部门id',
  `parent_id` bigint(0) NULL DEFAULT 0 COMMENT '父部门id',
  `ancestors` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '' COMMENT '祖级列表',
  `dept_name` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '' COMMENT '部门名称',
  `order_num` int(0) NULL DEFAULT 0 COMMENT '显示顺序',
  `leader` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '负责人',
  `phone` varchar(11) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '联系电话',
  `email` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '邮箱',
  `status` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '0' COMMENT '部门状态（0正常 1停用）',
  `del_flag` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '0' COMMENT '删除标志（0代表存在 2代表删除）',
  `create_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`dept_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 200 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci COMMENT = '部门表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of sys_dept
-- ----------------------------
INSERT INTO `sys_dept` VALUES (100, 0, '0', '清眸物联', 0, '清眸', '15888888888', 'tsingeye@qq.com', '0', '0', 'admin', '2022-04-15 10:31:06', 'admin', '2022-04-15 11:26:57');
INSERT INTO `sys_dept` VALUES (101, 100, '0,100', '合肥分公司', 1, '青橙', '15888888888', 'qingcheng@qq.com', '0', '0', 'admin', '2022-04-15 10:31:06', 'admin', '2022-04-15 11:27:32');
INSERT INTO `sys_dept` VALUES (102, 100, '0,100', '南京分公司', 2, '齐河', '15888888888', 'qihe@qq.com', '0', '0', 'admin', '2022-04-15 10:31:06', 'admin', '2022-04-15 11:27:53');
INSERT INTO `sys_dept` VALUES (103, 101, '0,100,101', '研发部门', 1, '若依', '15888888888', 'ry@qq.com', '0', '0', 'admin', '2022-04-15 10:31:06', '', NULL);
INSERT INTO `sys_dept` VALUES (104, 101, '0,100,101', '市场部门', 2, '若依', '15888888888', 'ry@qq.com', '0', '0', 'admin', '2022-04-15 10:31:06', '', NULL);
INSERT INTO `sys_dept` VALUES (105, 101, '0,100,101', '测试部门', 3, '若依', '15888888888', 'ry@qq.com', '0', '0', 'admin', '2022-04-15 10:31:06', '', NULL);
INSERT INTO `sys_dept` VALUES (106, 101, '0,100,101', '财务部门', 4, '若依', '15888888888', 'ry@qq.com', '0', '0', 'admin', '2022-04-15 10:31:06', '', NULL);
INSERT INTO `sys_dept` VALUES (107, 101, '0,100,101', '运维部门', 5, '若依', '15888888888', 'ry@qq.com', '0', '0', 'admin', '2022-04-15 10:31:06', '', NULL);
INSERT INTO `sys_dept` VALUES (108, 102, '0,100,102', '市场部门', 1, '若依', '15888888888', 'ry@qq.com', '0', '0', 'admin', '2022-04-15 10:31:06', '', NULL);
INSERT INTO `sys_dept` VALUES (109, 102, '0,100,102', '财务部门', 2, '若依', '15888888888', 'ry@qq.com', '0', '0', 'admin', '2022-04-15 10:31:06', '', NULL);

-- ----------------------------
-- Table structure for sys_dict_data
-- ----------------------------
DROP TABLE IF EXISTS `sys_dict_data`;
CREATE TABLE `sys_dict_data`  (
  `dict_code` bigint(0) NOT NULL AUTO_INCREMENT COMMENT '字典编码',
  `dict_sort` int(0) NULL DEFAULT 0 COMMENT '字典排序',
  `dict_label` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '' COMMENT '字典标签',
  `dict_value` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '' COMMENT '字典键值',
  `dict_type` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '' COMMENT '字典类型',
  `css_class` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '样式属性（其他样式扩展）',
  `list_class` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '表格回显样式',
  `is_default` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT 'N' COMMENT '是否默认（Y是 N否）',
  `status` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '0' COMMENT '状态（0正常 1停用）',
  `create_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`dict_code`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 100 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci COMMENT = '字典数据表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of sys_dict_data
-- ----------------------------
INSERT INTO `sys_dict_data` VALUES (1, 1, '男', '0', 'sys_user_sex', '', '', 'Y', '0', 'admin', '2022-04-15 10:31:13', '', NULL, '性别男');
INSERT INTO `sys_dict_data` VALUES (2, 2, '女', '1', 'sys_user_sex', '', '', 'N', '0', 'admin', '2022-04-15 10:31:13', '', NULL, '性别女');
INSERT INTO `sys_dict_data` VALUES (3, 3, '未知', '2', 'sys_user_sex', '', '', 'N', '0', 'admin', '2022-04-15 10:31:13', '', NULL, '性别未知');
INSERT INTO `sys_dict_data` VALUES (4, 1, '显示', '0', 'sys_show_hide', '', 'primary', 'Y', '0', 'admin', '2022-04-15 10:31:13', '', NULL, '显示菜单');
INSERT INTO `sys_dict_data` VALUES (5, 2, '隐藏', '1', 'sys_show_hide', '', 'danger', 'N', '0', 'admin', '2022-04-15 10:31:13', '', NULL, '隐藏菜单');
INSERT INTO `sys_dict_data` VALUES (6, 1, '正常', '0', 'sys_normal_disable', '', 'primary', 'Y', '0', 'admin', '2022-04-15 10:31:13', '', NULL, '正常状态');
INSERT INTO `sys_dict_data` VALUES (7, 2, '停用', '1', 'sys_normal_disable', '', 'danger', 'N', '0', 'admin', '2022-04-15 10:31:13', '', NULL, '停用状态');
INSERT INTO `sys_dict_data` VALUES (8, 1, '正常', '0', 'sys_job_status', '', 'primary', 'Y', '0', 'admin', '2022-04-15 10:31:13', '', NULL, '正常状态');
INSERT INTO `sys_dict_data` VALUES (9, 2, '暂停', '1', 'sys_job_status', '', 'danger', 'N', '0', 'admin', '2022-04-15 10:31:13', '', NULL, '停用状态');
INSERT INTO `sys_dict_data` VALUES (10, 1, '默认', 'DEFAULT', 'sys_job_group', '', '', 'Y', '0', 'admin', '2022-04-15 10:31:13', '', NULL, '默认分组');
INSERT INTO `sys_dict_data` VALUES (11, 2, '系统', 'SYSTEM', 'sys_job_group', '', '', 'N', '0', 'admin', '2022-04-15 10:31:13', '', NULL, '系统分组');
INSERT INTO `sys_dict_data` VALUES (12, 1, '是', 'Y', 'sys_yes_no', '', 'primary', 'Y', '0', 'admin', '2022-04-15 10:31:13', '', NULL, '系统默认是');
INSERT INTO `sys_dict_data` VALUES (13, 2, '否', 'N', 'sys_yes_no', '', 'danger', 'N', '0', 'admin', '2022-04-15 10:31:13', '', NULL, '系统默认否');
INSERT INTO `sys_dict_data` VALUES (14, 1, '通知', '1', 'sys_notice_type', '', 'warning', 'Y', '0', 'admin', '2022-04-15 10:31:13', '', NULL, '通知');
INSERT INTO `sys_dict_data` VALUES (15, 2, '公告', '2', 'sys_notice_type', '', 'success', 'N', '0', 'admin', '2022-04-15 10:31:13', '', NULL, '公告');
INSERT INTO `sys_dict_data` VALUES (16, 1, '正常', '0', 'sys_notice_status', '', 'primary', 'Y', '0', 'admin', '2022-04-15 10:31:13', '', NULL, '正常状态');
INSERT INTO `sys_dict_data` VALUES (17, 2, '关闭', '1', 'sys_notice_status', '', 'danger', 'N', '0', 'admin', '2022-04-15 10:31:13', '', NULL, '关闭状态');
INSERT INTO `sys_dict_data` VALUES (18, 1, '新增', '1', 'sys_oper_type', '', 'info', 'N', '0', 'admin', '2022-04-15 10:31:13', '', NULL, '新增操作');
INSERT INTO `sys_dict_data` VALUES (19, 2, '修改', '2', 'sys_oper_type', '', 'info', 'N', '0', 'admin', '2022-04-15 10:31:13', '', NULL, '修改操作');
INSERT INTO `sys_dict_data` VALUES (20, 3, '删除', '3', 'sys_oper_type', '', 'danger', 'N', '0', 'admin', '2022-04-15 10:31:13', '', NULL, '删除操作');
INSERT INTO `sys_dict_data` VALUES (21, 4, '授权', '4', 'sys_oper_type', '', 'primary', 'N', '0', 'admin', '2022-04-15 10:31:13', '', NULL, '授权操作');
INSERT INTO `sys_dict_data` VALUES (22, 5, '导出', '5', 'sys_oper_type', '', 'warning', 'N', '0', 'admin', '2022-04-15 10:31:13', '', NULL, '导出操作');
INSERT INTO `sys_dict_data` VALUES (23, 6, '导入', '6', 'sys_oper_type', '', 'warning', 'N', '0', 'admin', '2022-04-15 10:31:13', '', NULL, '导入操作');
INSERT INTO `sys_dict_data` VALUES (24, 7, '强退', '7', 'sys_oper_type', '', 'danger', 'N', '0', 'admin', '2022-04-15 10:31:13', '', NULL, '强退操作');
INSERT INTO `sys_dict_data` VALUES (25, 8, '生成代码', '8', 'sys_oper_type', '', 'warning', 'N', '0', 'admin', '2022-04-15 10:31:13', '', NULL, '生成操作');
INSERT INTO `sys_dict_data` VALUES (26, 9, '清空数据', '9', 'sys_oper_type', '', 'danger', 'N', '0', 'admin', '2022-04-15 10:31:13', '', NULL, '清空操作');
INSERT INTO `sys_dict_data` VALUES (27, 1, '成功', '0', 'sys_common_status', '', 'primary', 'N', '0', 'admin', '2022-04-15 10:31:13', '', NULL, '正常状态');
INSERT INTO `sys_dict_data` VALUES (28, 2, '失败', '1', 'sys_common_status', '', 'danger', 'N', '0', 'admin', '2022-04-15 10:31:13', '', NULL, '停用状态');

-- ----------------------------
-- Table structure for sys_dict_type
-- ----------------------------
DROP TABLE IF EXISTS `sys_dict_type`;
CREATE TABLE `sys_dict_type`  (
  `dict_id` bigint(0) NOT NULL AUTO_INCREMENT COMMENT '字典主键',
  `dict_name` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '' COMMENT '字典名称',
  `dict_type` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '' COMMENT '字典类型',
  `status` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '0' COMMENT '状态（0正常 1停用）',
  `create_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`dict_id`) USING BTREE,
  UNIQUE INDEX `dict_type`(`dict_type`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 100 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci COMMENT = '字典类型表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of sys_dict_type
-- ----------------------------
INSERT INTO `sys_dict_type` VALUES (1, '用户性别', 'sys_user_sex', '0', 'admin', '2022-04-15 10:31:12', '', NULL, '用户性别列表');
INSERT INTO `sys_dict_type` VALUES (2, '菜单状态', 'sys_show_hide', '0', 'admin', '2022-04-15 10:31:12', '', NULL, '菜单状态列表');
INSERT INTO `sys_dict_type` VALUES (3, '系统开关', 'sys_normal_disable', '0', 'admin', '2022-04-15 10:31:12', '', NULL, '系统开关列表');
INSERT INTO `sys_dict_type` VALUES (4, '任务状态', 'sys_job_status', '0', 'admin', '2022-04-15 10:31:12', '', NULL, '任务状态列表');
INSERT INTO `sys_dict_type` VALUES (5, '任务分组', 'sys_job_group', '0', 'admin', '2022-04-15 10:31:12', '', NULL, '任务分组列表');
INSERT INTO `sys_dict_type` VALUES (6, '系统是否', 'sys_yes_no', '0', 'admin', '2022-04-15 10:31:12', '', NULL, '系统是否列表');
INSERT INTO `sys_dict_type` VALUES (7, '通知类型', 'sys_notice_type', '0', 'admin', '2022-04-15 10:31:12', '', NULL, '通知类型列表');
INSERT INTO `sys_dict_type` VALUES (8, '通知状态', 'sys_notice_status', '0', 'admin', '2022-04-15 10:31:12', '', NULL, '通知状态列表');
INSERT INTO `sys_dict_type` VALUES (9, '操作类型', 'sys_oper_type', '0', 'admin', '2022-04-15 10:31:12', '', NULL, '操作类型列表');
INSERT INTO `sys_dict_type` VALUES (10, '系统状态', 'sys_common_status', '0', 'admin', '2022-04-15 10:31:12', '', NULL, '登录状态列表');

-- ----------------------------
-- Table structure for sys_job
-- ----------------------------
DROP TABLE IF EXISTS `sys_job`;
CREATE TABLE `sys_job`  (
  `job_id` bigint(0) NOT NULL AUTO_INCREMENT COMMENT '任务ID',
  `job_name` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL DEFAULT '' COMMENT '任务名称',
  `job_group` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL DEFAULT 'DEFAULT' COMMENT '任务组名',
  `invoke_target` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '调用目标字符串',
  `cron_expression` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '' COMMENT 'cron执行表达式',
  `misfire_policy` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '3' COMMENT '计划执行错误策略（1立即执行 2执行一次 3放弃执行）',
  `concurrent` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '1' COMMENT '是否并发执行（0允许 1禁止）',
  `status` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '0' COMMENT '状态（0正常 1暂停）',
  `create_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '' COMMENT '备注信息',
  PRIMARY KEY (`job_id`, `job_name`, `job_group`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 101 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci COMMENT = '定时任务调度表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of sys_job
-- ----------------------------
INSERT INTO `sys_job` VALUES (1, '系统默认（无参）', 'DEFAULT', 'tsingeyeTask.tsNoParams', '0/10 * * * * ?', '3', '1', '1', 'admin', '2022-04-15 10:31:15', 'admin', '2022-04-15 11:32:58', '');
INSERT INTO `sys_job` VALUES (2, '系统默认（有参）', 'DEFAULT', 'tsingeyeTask.tsParams(\'ry\')', '0/15 * * * * ?', '3', '1', '1', 'admin', '2022-04-15 10:31:15', 'admin', '2022-04-15 11:33:07', '');
INSERT INTO `sys_job` VALUES (3, '系统默认（多参）', 'DEFAULT', 'tsingeyeTask.tsMultipleParams(\'ry\', true, 2000L, 316.50D, 100)', '0/20 * * * * ?', '3', '1', '1', 'admin', '2022-04-15 10:31:15', 'admin', '2022-04-15 11:33:14', '');
INSERT INTO `sys_job` VALUES (100, '定时刷新是否下线', 'DEFAULT', 'viidTask.offLineGudgment', '* * * * * ?', '1', '1', '1', 'admin', '2022-06-27 10:02:59', '', NULL, '');

-- ----------------------------
-- Table structure for sys_job_log
-- ----------------------------
DROP TABLE IF EXISTS `sys_job_log`;
CREATE TABLE `sys_job_log`  (
  `job_log_id` bigint(0) NOT NULL AUTO_INCREMENT COMMENT '任务日志ID',
  `job_name` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '任务名称',
  `job_group` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '任务组名',
  `invoke_target` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '调用目标字符串',
  `job_message` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '日志信息',
  `status` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '0' COMMENT '执行状态（0正常 1失败）',
  `exception_info` varchar(2000) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '' COMMENT '异常信息',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`job_log_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 5 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci COMMENT = '定时任务调度日志表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of sys_job_log
-- ----------------------------
INSERT INTO `sys_job_log` VALUES (1, '定时刷新是否下线', 'DEFAULT', 'viidTask.offLineGudgment', '定时刷新是否下线 总共耗时：760毫秒', '0', '', '2022-06-27 10:03:19');
INSERT INTO `sys_job_log` VALUES (2, '定时刷新是否下线', 'DEFAULT', 'viidTask.offLineGudgment', '定时刷新是否下线 总共耗时：799毫秒', '0', '', '2022-06-28 10:55:20');
INSERT INTO `sys_job_log` VALUES (3, '定时刷新是否下线', 'DEFAULT', 'viidTask.offLineGudgment', '定时刷新是否下线 总共耗时：1257毫秒', '0', '', '2022-06-28 13:44:15');
INSERT INTO `sys_job_log` VALUES (4, '定时刷新是否下线', 'DEFAULT', 'viidTask.offLineGudgment', '定时刷新是否下线 总共耗时：212毫秒', '0', '', '2022-08-18 17:29:58');

-- ----------------------------
-- Table structure for sys_logininfor
-- ----------------------------
DROP TABLE IF EXISTS `sys_logininfor`;
CREATE TABLE `sys_logininfor`  (
  `info_id` bigint(0) NOT NULL AUTO_INCREMENT COMMENT '访问ID',
  `user_name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '' COMMENT '用户账号',
  `ipaddr` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '' COMMENT '登录IP地址',
  `login_location` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '' COMMENT '登录地点',
  `browser` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '' COMMENT '浏览器类型',
  `os` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '' COMMENT '操作系统',
  `status` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '0' COMMENT '登录状态（0成功 1失败）',
  `msg` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '' COMMENT '提示消息',
  `login_time` datetime(0) NULL DEFAULT NULL COMMENT '访问时间',
  PRIMARY KEY (`info_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 387 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci COMMENT = '系统访问记录' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of sys_logininfor
-- ----------------------------

-- ----------------------------
-- Table structure for sys_menu
-- ----------------------------
DROP TABLE IF EXISTS `sys_menu`;
CREATE TABLE `sys_menu`  (
  `menu_id` bigint(0) NOT NULL AUTO_INCREMENT COMMENT '菜单ID',
  `menu_name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '菜单名称',
  `parent_id` bigint(0) NULL DEFAULT 0 COMMENT '父菜单ID',
  `order_num` int(0) NULL DEFAULT 0 COMMENT '显示顺序',
  `path` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '' COMMENT '路由地址',
  `component` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '组件路径',
  `query` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '路由参数',
  `is_frame` int(0) NULL DEFAULT 1 COMMENT '是否为外链（0是 1否）',
  `is_cache` int(0) NULL DEFAULT 0 COMMENT '是否缓存（0缓存 1不缓存）',
  `menu_type` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '' COMMENT '菜单类型（M目录 C菜单 F按钮）',
  `visible` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '0' COMMENT '菜单状态（0显示 1隐藏）',
  `status` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '0' COMMENT '菜单状态（0正常 1停用）',
  `perms` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '权限标识',
  `icon` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '#' COMMENT '菜单图标',
  `create_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '' COMMENT '备注',
  PRIMARY KEY (`menu_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2053 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci COMMENT = '菜单权限表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of sys_menu
-- ----------------------------
INSERT INTO `sys_menu` VALUES (1, '系统管理', 0, 8, 'system', NULL, '', 1, 0, 'M', '0', '0', '', 'system', 'admin', '2022-04-15 10:31:09', 'admin', '2022-09-02 20:33:36', '系统管理目录');
INSERT INTO `sys_menu` VALUES (2, '系统监控', 0, 6, 'monitor', NULL, '', 1, 0, 'M', '0', '0', '', 'monitor', 'admin', '2022-04-15 10:31:09', 'admin', '2022-04-22 11:33:10', '系统监控目录');
INSERT INTO `sys_menu` VALUES (3, '系统工具', 0, 7, 'tool', NULL, '', 1, 0, 'M', '0', '0', '', 'tool', 'admin', '2022-04-15 10:31:09', 'admin', '2022-04-22 11:33:16', '系统工具目录');
INSERT INTO `sys_menu` VALUES (100, '用户管理', 1, 1, 'user', 'system/user/index', '', 1, 0, 'C', '0', '0', 'system:user:list', 'user', 'admin', '2022-04-15 10:31:09', '', NULL, '用户管理菜单');
INSERT INTO `sys_menu` VALUES (101, '角色管理', 1, 2, 'role', 'system/role/index', '', 1, 0, 'C', '0', '0', 'system:role:list', 'peoples', 'admin', '2022-04-15 10:31:09', '', NULL, '角色管理菜单');
INSERT INTO `sys_menu` VALUES (102, '菜单管理', 1, 3, 'menu', 'system/menu/index', '', 1, 0, 'C', '0', '0', 'system:menu:list', 'tree-table', 'admin', '2022-04-15 10:31:09', '', NULL, '菜单管理菜单');
INSERT INTO `sys_menu` VALUES (103, '部门管理', 1, 4, 'dept', 'system/dept/index', '', 1, 0, 'C', '0', '0', 'system:dept:list', 'tree', 'admin', '2022-04-15 10:31:09', '', NULL, '部门管理菜单');
INSERT INTO `sys_menu` VALUES (104, '岗位管理', 1, 5, 'post', 'system/post/index', '', 1, 0, 'C', '0', '0', 'system:post:list', 'post', 'admin', '2022-04-15 10:31:09', '', NULL, '岗位管理菜单');
INSERT INTO `sys_menu` VALUES (105, '字典管理', 1, 6, 'dict', 'system/dict/index', '', 1, 0, 'C', '0', '0', 'system:dict:list', 'dict', 'admin', '2022-04-15 10:31:09', '', NULL, '字典管理菜单');
INSERT INTO `sys_menu` VALUES (106, '参数设置', 1, 7, 'config', 'system/config/index', '', 1, 0, 'C', '0', '0', 'system:config:list', 'edit', 'admin', '2022-04-15 10:31:09', '', NULL, '参数设置菜单');
INSERT INTO `sys_menu` VALUES (107, '通知公告', 1, 8, 'notice', 'system/notice/index', '', 1, 0, 'C', '0', '0', 'system:notice:list', 'message', 'admin', '2022-04-15 10:31:09', '', NULL, '通知公告菜单');
INSERT INTO `sys_menu` VALUES (108, '日志管理', 1, 9, 'log', '', '', 1, 0, 'M', '0', '0', '', 'log', 'admin', '2022-04-15 10:31:09', '', NULL, '日志管理菜单');
INSERT INTO `sys_menu` VALUES (109, '在线用户', 2, 1, 'online', 'monitor/online/index', '', 1, 0, 'C', '0', '0', 'monitor:online:list', 'online', 'admin', '2022-04-15 10:31:09', '', NULL, '在线用户菜单');
INSERT INTO `sys_menu` VALUES (110, '定时任务', 2, 2, 'job', 'monitor/job/index', '', 1, 0, 'C', '0', '0', 'monitor:job:list', 'job', 'admin', '2022-04-15 10:31:09', '', NULL, '定时任务菜单');
INSERT INTO `sys_menu` VALUES (111, '数据监控', 2, 3, 'druid', 'monitor/druid/index', '', 1, 0, 'C', '0', '0', 'monitor:druid:list', 'druid', 'admin', '2022-04-15 10:31:09', '', NULL, '数据监控菜单');
INSERT INTO `sys_menu` VALUES (112, '服务监控', 2, 4, 'server', 'monitor/server/index', '', 1, 0, 'C', '0', '0', 'monitor:server:list', 'server', 'admin', '2022-04-15 10:31:09', '', NULL, '服务监控菜单');
INSERT INTO `sys_menu` VALUES (113, '缓存监控', 2, 5, 'cache', 'monitor/cache/index', '', 1, 0, 'C', '0', '0', 'monitor:cache:list', 'redis', 'admin', '2022-04-15 10:31:09', '', NULL, '缓存监控菜单');
INSERT INTO `sys_menu` VALUES (114, '表单构建', 3, 1, 'build', 'tool/build/index', '', 1, 0, 'C', '0', '0', 'tool:build:list', 'build', 'admin', '2022-04-15 10:31:09', '', NULL, '表单构建菜单');
INSERT INTO `sys_menu` VALUES (115, '代码生成', 3, 2, 'gen', 'tool/gen/index', '', 1, 0, 'C', '0', '0', 'tool:gen:list', 'code', 'admin', '2022-04-15 10:31:09', '', NULL, '代码生成菜单');
INSERT INTO `sys_menu` VALUES (116, '系统接口', 3, 3, 'swagger', 'tool/swagger/index', '', 1, 0, 'C', '0', '0', 'tool:swagger:list', 'swagger', 'admin', '2022-04-15 10:31:09', '', NULL, '系统接口菜单');
INSERT INTO `sys_menu` VALUES (500, '操作日志', 108, 1, 'operlog', 'monitor/operlog/index', '', 1, 0, 'C', '0', '0', 'monitor:operlog:list', 'form', 'admin', '2022-04-15 10:31:09', '', NULL, '操作日志菜单');
INSERT INTO `sys_menu` VALUES (501, '登录日志', 108, 2, 'logininfor', 'monitor/logininfor/index', '', 1, 0, 'C', '0', '0', 'monitor:logininfor:list', 'logininfor', 'admin', '2022-04-15 10:31:09', '', NULL, '登录日志菜单');
INSERT INTO `sys_menu` VALUES (1001, '用户查询', 100, 1, '', '', '', 1, 0, 'F', '0', '0', 'system:user:query', '#', 'admin', '2022-04-15 10:31:09', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1002, '用户新增', 100, 2, '', '', '', 1, 0, 'F', '0', '0', 'system:user:add', '#', 'admin', '2022-04-15 10:31:09', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1003, '用户修改', 100, 3, '', '', '', 1, 0, 'F', '0', '0', 'system:user:edit', '#', 'admin', '2022-04-15 10:31:09', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1004, '用户删除', 100, 4, '', '', '', 1, 0, 'F', '0', '0', 'system:user:remove', '#', 'admin', '2022-04-15 10:31:09', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1005, '用户导出', 100, 5, '', '', '', 1, 0, 'F', '0', '0', 'system:user:export', '#', 'admin', '2022-04-15 10:31:09', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1006, '用户导入', 100, 6, '', '', '', 1, 0, 'F', '0', '0', 'system:user:import', '#', 'admin', '2022-04-15 10:31:09', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1007, '重置密码', 100, 7, '', '', '', 1, 0, 'F', '0', '0', 'system:user:resetPwd', '#', 'admin', '2022-04-15 10:31:09', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1008, '角色查询', 101, 1, '', '', '', 1, 0, 'F', '0', '0', 'system:role:query', '#', 'admin', '2022-04-15 10:31:09', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1009, '角色新增', 101, 2, '', '', '', 1, 0, 'F', '0', '0', 'system:role:add', '#', 'admin', '2022-04-15 10:31:09', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1010, '角色修改', 101, 3, '', '', '', 1, 0, 'F', '0', '0', 'system:role:edit', '#', 'admin', '2022-04-15 10:31:09', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1011, '角色删除', 101, 4, '', '', '', 1, 0, 'F', '0', '0', 'system:role:remove', '#', 'admin', '2022-04-15 10:31:09', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1012, '角色导出', 101, 5, '', '', '', 1, 0, 'F', '0', '0', 'system:role:export', '#', 'admin', '2022-04-15 10:31:09', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1013, '菜单查询', 102, 1, '', '', '', 1, 0, 'F', '0', '0', 'system:menu:query', '#', 'admin', '2022-04-15 10:31:09', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1014, '菜单新增', 102, 2, '', '', '', 1, 0, 'F', '0', '0', 'system:menu:add', '#', 'admin', '2022-04-15 10:31:09', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1015, '菜单修改', 102, 3, '', '', '', 1, 0, 'F', '0', '0', 'system:menu:edit', '#', 'admin', '2022-04-15 10:31:09', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1016, '菜单删除', 102, 4, '', '', '', 1, 0, 'F', '0', '0', 'system:menu:remove', '#', 'admin', '2022-04-15 10:31:09', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1017, '部门查询', 103, 1, '', '', '', 1, 0, 'F', '0', '0', 'system:dept:query', '#', 'admin', '2022-04-15 10:31:09', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1018, '部门新增', 103, 2, '', '', '', 1, 0, 'F', '0', '0', 'system:dept:add', '#', 'admin', '2022-04-15 10:31:09', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1019, '部门修改', 103, 3, '', '', '', 1, 0, 'F', '0', '0', 'system:dept:edit', '#', 'admin', '2022-04-15 10:31:09', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1020, '部门删除', 103, 4, '', '', '', 1, 0, 'F', '0', '0', 'system:dept:remove', '#', 'admin', '2022-04-15 10:31:09', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1021, '岗位查询', 104, 1, '', '', '', 1, 0, 'F', '0', '0', 'system:post:query', '#', 'admin', '2022-04-15 10:31:09', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1022, '岗位新增', 104, 2, '', '', '', 1, 0, 'F', '0', '0', 'system:post:add', '#', 'admin', '2022-04-15 10:31:09', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1023, '岗位修改', 104, 3, '', '', '', 1, 0, 'F', '0', '0', 'system:post:edit', '#', 'admin', '2022-04-15 10:31:09', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1024, '岗位删除', 104, 4, '', '', '', 1, 0, 'F', '0', '0', 'system:post:remove', '#', 'admin', '2022-04-15 10:31:09', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1025, '岗位导出', 104, 5, '', '', '', 1, 0, 'F', '0', '0', 'system:post:export', '#', 'admin', '2022-04-15 10:31:09', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1026, '字典查询', 105, 1, '#', '', '', 1, 0, 'F', '0', '0', 'system:dict:query', '#', 'admin', '2022-04-15 10:31:09', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1027, '字典新增', 105, 2, '#', '', '', 1, 0, 'F', '0', '0', 'system:dict:add', '#', 'admin', '2022-04-15 10:31:09', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1028, '字典修改', 105, 3, '#', '', '', 1, 0, 'F', '0', '0', 'system:dict:edit', '#', 'admin', '2022-04-15 10:31:09', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1029, '字典删除', 105, 4, '#', '', '', 1, 0, 'F', '0', '0', 'system:dict:remove', '#', 'admin', '2022-04-15 10:31:09', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1030, '字典导出', 105, 5, '#', '', '', 1, 0, 'F', '0', '0', 'system:dict:export', '#', 'admin', '2022-04-15 10:31:09', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1031, '参数查询', 106, 1, '#', '', '', 1, 0, 'F', '0', '0', 'system:config:query', '#', 'admin', '2022-04-15 10:31:09', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1032, '参数新增', 106, 2, '#', '', '', 1, 0, 'F', '0', '0', 'system:config:add', '#', 'admin', '2022-04-15 10:31:09', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1033, '参数修改', 106, 3, '#', '', '', 1, 0, 'F', '0', '0', 'system:config:edit', '#', 'admin', '2022-04-15 10:31:09', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1034, '参数删除', 106, 4, '#', '', '', 1, 0, 'F', '0', '0', 'system:config:remove', '#', 'admin', '2022-04-15 10:31:09', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1035, '参数导出', 106, 5, '#', '', '', 1, 0, 'F', '0', '0', 'system:config:export', '#', 'admin', '2022-04-15 10:31:09', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1036, '公告查询', 107, 1, '#', '', '', 1, 0, 'F', '0', '0', 'system:notice:query', '#', 'admin', '2022-04-15 10:31:09', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1037, '公告新增', 107, 2, '#', '', '', 1, 0, 'F', '0', '0', 'system:notice:add', '#', 'admin', '2022-04-15 10:31:09', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1038, '公告修改', 107, 3, '#', '', '', 1, 0, 'F', '0', '0', 'system:notice:edit', '#', 'admin', '2022-04-15 10:31:09', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1039, '公告删除', 107, 4, '#', '', '', 1, 0, 'F', '0', '0', 'system:notice:remove', '#', 'admin', '2022-04-15 10:31:09', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1040, '操作查询', 500, 1, '#', '', '', 1, 0, 'F', '0', '0', 'monitor:operlog:query', '#', 'admin', '2022-04-15 10:31:09', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1041, '操作删除', 500, 2, '#', '', '', 1, 0, 'F', '0', '0', 'monitor:operlog:remove', '#', 'admin', '2022-04-15 10:31:09', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1042, '日志导出', 500, 4, '#', '', '', 1, 0, 'F', '0', '0', 'monitor:operlog:export', '#', 'admin', '2022-04-15 10:31:09', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1043, '登录查询', 501, 1, '#', '', '', 1, 0, 'F', '0', '0', 'monitor:logininfor:query', '#', 'admin', '2022-04-15 10:31:09', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1044, '登录删除', 501, 2, '#', '', '', 1, 0, 'F', '0', '0', 'monitor:logininfor:remove', '#', 'admin', '2022-04-15 10:31:09', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1045, '日志导出', 501, 3, '#', '', '', 1, 0, 'F', '0', '0', 'monitor:logininfor:export', '#', 'admin', '2022-04-15 10:31:09', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1046, '在线查询', 109, 1, '#', '', '', 1, 0, 'F', '0', '0', 'monitor:online:query', '#', 'admin', '2022-04-15 10:31:09', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1047, '批量强退', 109, 2, '#', '', '', 1, 0, 'F', '0', '0', 'monitor:online:batchLogout', '#', 'admin', '2022-04-15 10:31:09', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1048, '单条强退', 109, 3, '#', '', '', 1, 0, 'F', '0', '0', 'monitor:online:forceLogout', '#', 'admin', '2022-04-15 10:31:09', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1049, '任务查询', 110, 1, '#', '', '', 1, 0, 'F', '0', '0', 'monitor:job:query', '#', 'admin', '2022-04-15 10:31:09', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1050, '任务新增', 110, 2, '#', '', '', 1, 0, 'F', '0', '0', 'monitor:job:add', '#', 'admin', '2022-04-15 10:31:09', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1051, '任务修改', 110, 3, '#', '', '', 1, 0, 'F', '0', '0', 'monitor:job:edit', '#', 'admin', '2022-04-15 10:31:09', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1052, '任务删除', 110, 4, '#', '', '', 1, 0, 'F', '0', '0', 'monitor:job:remove', '#', 'admin', '2022-04-15 10:31:09', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1053, '状态修改', 110, 5, '#', '', '', 1, 0, 'F', '0', '0', 'monitor:job:changeStatus', '#', 'admin', '2022-04-15 10:31:09', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1054, '任务导出', 110, 7, '#', '', '', 1, 0, 'F', '0', '0', 'monitor:job:export', '#', 'admin', '2022-04-15 10:31:09', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1055, '生成查询', 115, 1, '#', '', '', 1, 0, 'F', '0', '0', 'tool:gen:query', '#', 'admin', '2022-04-15 10:31:09', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1056, '生成修改', 115, 2, '#', '', '', 1, 0, 'F', '0', '0', 'tool:gen:edit', '#', 'admin', '2022-04-15 10:31:09', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1057, '生成删除', 115, 3, '#', '', '', 1, 0, 'F', '0', '0', 'tool:gen:remove', '#', 'admin', '2022-04-15 10:31:09', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1058, '导入代码', 115, 2, '#', '', '', 1, 0, 'F', '0', '0', 'tool:gen:import', '#', 'admin', '2022-04-15 10:31:09', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1059, '预览代码', 115, 4, '#', '', '', 1, 0, 'F', '0', '0', 'tool:gen:preview', '#', 'admin', '2022-04-15 10:31:09', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1060, '生成代码', 115, 5, '#', '', '', 1, 0, 'F', '0', '0', 'tool:gen:code', '#', 'admin', '2022-04-15 10:31:09', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2007, '人脸管理', 0, 3, 'face', NULL, NULL, 1, 0, 'M', '0', '0', '', 'face-id', 'admin', '2022-04-21 16:05:33', 'admin', '2022-04-22 11:33:23', '');
INSERT INTO `sys_menu` VALUES (2008, '人脸信息', 2007, 1, 'info', 'viid/face/index', NULL, 1, 0, 'C', '0', '0', 'viid:face:list', 'info', 'admin', '2022-04-21 16:09:34', 'admin', '2022-04-22 15:50:02', '人脸信息菜单');
INSERT INTO `sys_menu` VALUES (2009, '人脸信息查询', 2008, 1, '#', '', NULL, 1, 0, 'F', '0', '0', 'viid:face:query', '#', 'admin', '2022-04-21 16:09:34', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2010, '人脸信息新增', 2008, 2, '#', '', NULL, 1, 0, 'F', '0', '0', 'viid:face:add', '#', 'admin', '2022-04-21 16:09:34', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2011, '人脸信息修改', 2008, 3, '#', '', NULL, 1, 0, 'F', '0', '0', 'viid:face:edit', '#', 'admin', '2022-04-21 16:09:34', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2012, '人脸信息删除', 2008, 4, '#', '', NULL, 1, 0, 'F', '0', '0', 'viid:face:remove', '#', 'admin', '2022-04-21 16:09:35', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2013, '人脸信息导出', 2008, 5, '#', '', NULL, 1, 0, 'F', '0', '0', 'viid:face:export', '#', 'admin', '2022-04-21 16:09:35', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2014, '人脸照片', 2007, 1, 'image', 'viid/image/index', NULL, 1, 0, 'C', '0', '0', 'viid:image:list', 'ic_face', 'admin', '2022-04-21 16:52:30', 'admin', '2022-04-21 18:34:56', '人脸照片菜单');
INSERT INTO `sys_menu` VALUES (2015, '人脸照片查询', 2014, 1, '#', '', NULL, 1, 0, 'F', '0', '0', 'viid:image:query', '#', 'admin', '2022-04-21 16:52:30', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2016, '人脸照片新增', 2014, 2, '#', '', NULL, 1, 0, 'F', '0', '0', 'viid:image:add', '#', 'admin', '2022-04-21 16:52:30', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2017, '人脸照片修改', 2014, 3, '#', '', NULL, 1, 0, 'F', '0', '0', 'viid:image:edit', '#', 'admin', '2022-04-21 16:52:30', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2018, '人脸照片删除', 2014, 4, '#', '', NULL, 1, 0, 'F', '0', '0', 'viid:image:remove', '#', 'admin', '2022-04-21 16:52:30', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2019, '人脸照片导出', 2014, 5, '#', '', NULL, 1, 0, 'F', '0', '0', 'viid:image:export', '#', 'admin', '2022-04-21 16:52:30', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2020, '设备管理', 0, 2, 'device', NULL, NULL, 1, 0, 'M', '0', '0', '', 'devices', 'admin', '2022-04-22 11:32:50', 'admin', '2022-04-22 11:56:19', '');
INSERT INTO `sys_menu` VALUES (2021, '设备信息', 2020, 1, 'info', 'viid/device/index', NULL, 1, 0, 'C', '0', '0', 'viid:device:list', 'devices', 'admin', '2022-04-22 11:34:23', 'admin', '2022-04-22 15:50:29', '设备信息菜单');
INSERT INTO `sys_menu` VALUES (2022, '设备信息查询', 2021, 1, '#', '', NULL, 1, 0, 'F', '0', '0', 'viid:device:query', '#', 'admin', '2022-04-22 11:34:23', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2023, '设备信息新增', 2021, 2, '#', '', NULL, 1, 0, 'F', '0', '0', 'viid:device:add', '#', 'admin', '2022-04-22 11:34:23', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2024, '设备信息修改', 2021, 3, '#', '', NULL, 1, 0, 'F', '0', '0', 'viid:device:edit', '#', 'admin', '2022-04-22 11:34:24', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2025, '设备信息删除', 2021, 4, '#', '', NULL, 1, 0, 'F', '0', '0', 'viid:device:remove', '#', 'admin', '2022-04-22 11:34:24', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2026, '设备信息导出', 2021, 5, '#', '', NULL, 1, 0, 'F', '0', '0', 'viid:device:export', '#', 'admin', '2022-04-22 11:34:24', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2027, '车辆管理', 0, 5, 'vehicle', NULL, NULL, 1, 0, 'M', '0', '0', '', '车辆', 'admin', '2022-06-27 16:10:15', 'admin', '2022-06-29 10:46:41', '');
INSERT INTO `sys_menu` VALUES (2028, '机动车信息', 2027, 1, 'vehicle', 'viid/vehicle/index', NULL, 1, 0, 'C', '0', '0', 'viid:vehicle:list', 'info', 'admin', '2022-06-27 16:34:46', 'admin', '2022-06-27 17:30:47', '机动车信息菜单');
INSERT INTO `sys_menu` VALUES (2029, '机动车信息查询', 2028, 1, '#', '', NULL, 1, 0, 'F', '0', '0', 'viid:vehicle:query', '#', 'admin', '2022-06-27 16:34:46', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2030, '机动车信息新增', 2028, 2, '#', '', NULL, 1, 0, 'F', '0', '0', 'viid:vehicle:add', '#', 'admin', '2022-06-27 16:34:46', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2031, '机动车信息修改', 2028, 3, '#', '', NULL, 1, 0, 'F', '0', '0', 'viid:vehicle:edit', '#', 'admin', '2022-06-27 16:34:46', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2032, '机动车信息删除', 2028, 4, '#', '', NULL, 1, 0, 'F', '0', '0', 'viid:vehicle:remove', '#', 'admin', '2022-06-27 16:34:46', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2033, '机动车信息导出', 2028, 5, '#', '', NULL, 1, 0, 'F', '0', '0', 'viid:vehicle:export', '#', 'admin', '2022-06-27 16:34:46', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2034, '机动车照片', 2027, 1, 'vehicleImage', 'viid/vehicleImage/index', NULL, 1, 0, 'C', '0', '0', 'viid:vehicleImage:list', '车辆', 'admin', '2022-06-27 17:00:57', 'admin', '2022-06-28 11:05:46', '机动车图片菜单');
INSERT INTO `sys_menu` VALUES (2035, '机动车图片查询', 2034, 1, '#', '', NULL, 1, 0, 'F', '0', '0', 'viid:vehicleImage:query', '#', 'admin', '2022-06-27 17:00:57', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2036, '机动车图片新增', 2034, 2, '#', '', NULL, 1, 0, 'F', '0', '0', 'viid:vehicleImage:add', '#', 'admin', '2022-06-27 17:00:57', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2037, '机动车图片修改', 2034, 3, '#', '', NULL, 1, 0, 'F', '0', '0', 'viid:vehicleImage:edit', '#', 'admin', '2022-06-27 17:00:57', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2038, '机动车图片删除', 2034, 4, '#', '', NULL, 1, 0, 'F', '0', '0', 'viid:vehicleImage:remove', '#', 'admin', '2022-06-27 17:00:57', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2039, '机动车图片导出', 2034, 5, '#', '', NULL, 1, 0, 'F', '0', '0', 'viid:vehicleImage:export', '#', 'admin', '2022-06-27 17:00:57', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2040, '人体管理', 0, 4, 'person', NULL, NULL, 1, 0, 'M', '0', '0', '', '人体检测', 'admin', '2022-06-29 10:46:35', 'admin', '2022-06-29 14:31:14', '');
INSERT INTO `sys_menu` VALUES (2041, '人体信息', 2040, 1, 'person', 'viid/person/index', NULL, 1, 0, 'C', '0', '0', 'viid:person:list', 'info', 'admin', '2022-06-29 11:02:30', 'admin', '2022-06-29 14:31:38', '人体信息菜单');
INSERT INTO `sys_menu` VALUES (2042, '人体信息查询', 2041, 1, '#', '', NULL, 1, 0, 'F', '0', '0', 'viid:person:query', '#', 'admin', '2022-06-29 11:02:31', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2043, '人体信息新增', 2041, 2, '#', '', NULL, 1, 0, 'F', '0', '0', 'viid:person:add', '#', 'admin', '2022-06-29 11:02:31', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2044, '人体信息修改', 2041, 3, '#', '', NULL, 1, 0, 'F', '0', '0', 'viid:person:edit', '#', 'admin', '2022-06-29 11:02:31', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2045, '人体信息删除', 2041, 4, '#', '', NULL, 1, 0, 'F', '0', '0', 'viid:person:remove', '#', 'admin', '2022-06-29 11:02:31', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2046, '人体信息导出', 2041, 5, '#', '', NULL, 1, 0, 'F', '0', '0', 'viid:person:export', '#', 'admin', '2022-06-29 11:02:31', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2047, '人体照片', 2040, 1, 'personImage', 'viid/personImage/index', NULL, 1, 0, 'C', '0', '0', 'viid:personImage:list', '人体检测', 'admin', '2022-06-29 11:07:45', 'admin', '2022-06-29 14:31:44', '人体图片菜单');
INSERT INTO `sys_menu` VALUES (2048, '人体图片查询', 2047, 1, '#', '', NULL, 1, 0, 'F', '0', '0', 'viid:personImage:query', '#', 'admin', '2022-06-29 11:07:45', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2049, '人体图片新增', 2047, 2, '#', '', NULL, 1, 0, 'F', '0', '0', 'viid:personImage:add', '#', 'admin', '2022-06-29 11:07:46', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2050, '人体图片修改', 2047, 3, '#', '', NULL, 1, 0, 'F', '0', '0', 'viid:personImage:edit', '#', 'admin', '2022-06-29 11:07:46', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2051, '人体图片删除', 2047, 4, '#', '', NULL, 1, 0, 'F', '0', '0', 'viid:personImage:remove', '#', 'admin', '2022-06-29 11:07:46', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2052, '人体图片导出', 2047, 5, '#', '', NULL, 1, 0, 'F', '0', '0', 'viid:personImage:export', '#', 'admin', '2022-06-29 11:07:46', '', NULL, '');

-- ----------------------------
-- Table structure for sys_model_warehouse
-- ----------------------------
DROP TABLE IF EXISTS `sys_model_warehouse`;
CREATE TABLE `sys_model_warehouse`  (
  `id` bigint(0) NOT NULL,
  `model_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '模型名称',
  `model_address` varchar(512) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '模型地址',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '上传时间',
  `create_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '' COMMENT '创建人',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  `update_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '' COMMENT '更新人',
  `deleted` int(0) NOT NULL DEFAULT 0 COMMENT '0是未删除 1是删除',
  `remark` varchar(1024) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '' COMMENT '备注',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci COMMENT = '数据仓库' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of sys_model_warehouse
-- ----------------------------

-- ----------------------------
-- Table structure for sys_notice
-- ----------------------------
DROP TABLE IF EXISTS `sys_notice`;
CREATE TABLE `sys_notice`  (
  `notice_id` int(0) NOT NULL AUTO_INCREMENT COMMENT '公告ID',
  `notice_title` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '公告标题',
  `notice_type` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '公告类型（1通知 2公告）',
  `notice_content` longblob NULL COMMENT '公告内容',
  `status` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '0' COMMENT '公告状态（0正常 1关闭）',
  `create_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`notice_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 10 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci COMMENT = '通知公告表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of sys_notice
-- ----------------------------
INSERT INTO `sys_notice` VALUES (1, '温馨提醒：2018-07-01 若依新版本发布啦', '2', 0xE696B0E78988E69CACE58685E5AEB9, '0', 'admin', '2022-04-15 10:31:16', '', NULL, '管理员');
INSERT INTO `sys_notice` VALUES (2, '维护通知：2018-07-01 若依系统凌晨维护', '1', 0xE7BBB4E68AA4E58685E5AEB9, '0', 'admin', '2022-04-15 10:31:16', '', NULL, '管理员');

-- ----------------------------
-- Table structure for sys_oper_log
-- ----------------------------
DROP TABLE IF EXISTS `sys_oper_log`;
CREATE TABLE `sys_oper_log`  (
  `oper_id` bigint(0) NOT NULL AUTO_INCREMENT COMMENT '日志主键',
  `title` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '' COMMENT '模块标题',
  `business_type` int(0) NULL DEFAULT 0 COMMENT '业务类型（0其它 1新增 2修改 3删除）',
  `method` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '' COMMENT '方法名称',
  `request_method` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '' COMMENT '请求方式',
  `operator_type` int(0) NULL DEFAULT 0 COMMENT '操作类别（0其它 1后台用户 2手机端用户）',
  `oper_name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '' COMMENT '操作人员',
  `dept_name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '' COMMENT '部门名称',
  `oper_url` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '' COMMENT '请求URL',
  `oper_ip` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '' COMMENT '主机地址',
  `oper_location` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '' COMMENT '操作地点',
  `oper_param` varchar(2000) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '' COMMENT '请求参数',
  `json_result` varchar(2000) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '' COMMENT '返回参数',
  `status` int(0) NULL DEFAULT 0 COMMENT '操作状态（0正常 1异常）',
  `error_msg` varchar(2000) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '' COMMENT '错误消息',
  `oper_time` datetime(0) NULL DEFAULT NULL COMMENT '操作时间',
  PRIMARY KEY (`oper_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 232 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci COMMENT = '操作日志记录' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of sys_oper_log
-- ----------------------------

-- ----------------------------
-- Table structure for sys_post
-- ----------------------------
DROP TABLE IF EXISTS `sys_post`;
CREATE TABLE `sys_post`  (
  `post_id` bigint(0) NOT NULL AUTO_INCREMENT COMMENT '岗位ID',
  `post_code` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '岗位编码',
  `post_name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '岗位名称',
  `post_sort` int(0) NOT NULL COMMENT '显示顺序',
  `status` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '状态（0正常 1停用）',
  `create_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`post_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 5 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci COMMENT = '岗位信息表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of sys_post
-- ----------------------------
INSERT INTO `sys_post` VALUES (1, 'ceo', '董事长', 1, '0', 'admin', '2022-04-15 10:31:08', '', NULL, '');
INSERT INTO `sys_post` VALUES (2, 'se', '项目经理', 2, '0', 'admin', '2022-04-15 10:31:08', '', NULL, '');
INSERT INTO `sys_post` VALUES (3, 'hr', '人力资源', 3, '0', 'admin', '2022-04-15 10:31:08', '', NULL, '');
INSERT INTO `sys_post` VALUES (4, 'user', '普通员工', 4, '0', 'admin', '2022-04-15 10:31:08', '', NULL, '');

-- ----------------------------
-- Table structure for sys_role
-- ----------------------------
DROP TABLE IF EXISTS `sys_role`;
CREATE TABLE `sys_role`  (
  `role_id` bigint(0) NOT NULL AUTO_INCREMENT COMMENT '角色ID',
  `role_name` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '角色名称',
  `role_key` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '角色权限字符串',
  `role_sort` int(0) NOT NULL COMMENT '显示顺序',
  `data_scope` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '1' COMMENT '数据范围（1：全部数据权限 2：自定数据权限 3：本部门数据权限 4：本部门及以下数据权限）',
  `menu_check_strictly` tinyint(1) NULL DEFAULT 1 COMMENT '菜单树选择项是否关联显示',
  `dept_check_strictly` tinyint(1) NULL DEFAULT 1 COMMENT '部门树选择项是否关联显示',
  `status` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '角色状态（0正常 1停用）',
  `del_flag` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '0' COMMENT '删除标志（0代表存在 2代表删除）',
  `create_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`role_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 101 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci COMMENT = '角色信息表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of sys_role
-- ----------------------------
INSERT INTO `sys_role` VALUES (1, '超级管理员', 'admin', 1, '1', 1, 1, '0', '0', 'admin', '2022-04-15 10:31:08', '', NULL, '超级管理员');
INSERT INTO `sys_role` VALUES (2, '普通角色', 'common', 2, '2', 1, 1, '0', '0', 'admin', '2022-04-15 10:31:08', 'admin', '2022-04-15 14:34:53', '普通角色');
INSERT INTO `sys_role` VALUES (100, '分公司管理员', 'branchOfficeAdmin', 2, '4', 1, 1, '0', '0', 'admin', '2022-04-15 11:29:33', 'admin', '2022-04-15 14:34:57', NULL);

-- ----------------------------
-- Table structure for sys_role_dept
-- ----------------------------
DROP TABLE IF EXISTS `sys_role_dept`;
CREATE TABLE `sys_role_dept`  (
  `role_id` bigint(0) NOT NULL COMMENT '角色ID',
  `dept_id` bigint(0) NOT NULL COMMENT '部门ID',
  PRIMARY KEY (`role_id`, `dept_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci COMMENT = '角色和部门关联表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of sys_role_dept
-- ----------------------------
INSERT INTO `sys_role_dept` VALUES (2, 100);
INSERT INTO `sys_role_dept` VALUES (2, 101);
INSERT INTO `sys_role_dept` VALUES (2, 105);

-- ----------------------------
-- Table structure for sys_role_menu
-- ----------------------------
DROP TABLE IF EXISTS `sys_role_menu`;
CREATE TABLE `sys_role_menu`  (
  `role_id` bigint(0) NOT NULL COMMENT '角色ID',
  `menu_id` bigint(0) NOT NULL COMMENT '菜单ID',
  PRIMARY KEY (`role_id`, `menu_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci COMMENT = '角色和菜单关联表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of sys_role_menu
-- ----------------------------
INSERT INTO `sys_role_menu` VALUES (2, 1);
INSERT INTO `sys_role_menu` VALUES (2, 2);
INSERT INTO `sys_role_menu` VALUES (2, 3);
INSERT INTO `sys_role_menu` VALUES (2, 100);
INSERT INTO `sys_role_menu` VALUES (2, 101);
INSERT INTO `sys_role_menu` VALUES (2, 102);
INSERT INTO `sys_role_menu` VALUES (2, 103);
INSERT INTO `sys_role_menu` VALUES (2, 104);
INSERT INTO `sys_role_menu` VALUES (2, 105);
INSERT INTO `sys_role_menu` VALUES (2, 106);
INSERT INTO `sys_role_menu` VALUES (2, 107);
INSERT INTO `sys_role_menu` VALUES (2, 108);
INSERT INTO `sys_role_menu` VALUES (2, 109);
INSERT INTO `sys_role_menu` VALUES (2, 110);
INSERT INTO `sys_role_menu` VALUES (2, 111);
INSERT INTO `sys_role_menu` VALUES (2, 112);
INSERT INTO `sys_role_menu` VALUES (2, 113);
INSERT INTO `sys_role_menu` VALUES (2, 114);
INSERT INTO `sys_role_menu` VALUES (2, 115);
INSERT INTO `sys_role_menu` VALUES (2, 116);
INSERT INTO `sys_role_menu` VALUES (2, 500);
INSERT INTO `sys_role_menu` VALUES (2, 501);
INSERT INTO `sys_role_menu` VALUES (2, 1001);
INSERT INTO `sys_role_menu` VALUES (2, 1002);
INSERT INTO `sys_role_menu` VALUES (2, 1003);
INSERT INTO `sys_role_menu` VALUES (2, 1004);
INSERT INTO `sys_role_menu` VALUES (2, 1005);
INSERT INTO `sys_role_menu` VALUES (2, 1006);
INSERT INTO `sys_role_menu` VALUES (2, 1007);
INSERT INTO `sys_role_menu` VALUES (2, 1008);
INSERT INTO `sys_role_menu` VALUES (2, 1009);
INSERT INTO `sys_role_menu` VALUES (2, 1010);
INSERT INTO `sys_role_menu` VALUES (2, 1011);
INSERT INTO `sys_role_menu` VALUES (2, 1012);
INSERT INTO `sys_role_menu` VALUES (2, 1013);
INSERT INTO `sys_role_menu` VALUES (2, 1014);
INSERT INTO `sys_role_menu` VALUES (2, 1015);
INSERT INTO `sys_role_menu` VALUES (2, 1016);
INSERT INTO `sys_role_menu` VALUES (2, 1017);
INSERT INTO `sys_role_menu` VALUES (2, 1018);
INSERT INTO `sys_role_menu` VALUES (2, 1019);
INSERT INTO `sys_role_menu` VALUES (2, 1020);
INSERT INTO `sys_role_menu` VALUES (2, 1021);
INSERT INTO `sys_role_menu` VALUES (2, 1022);
INSERT INTO `sys_role_menu` VALUES (2, 1023);
INSERT INTO `sys_role_menu` VALUES (2, 1024);
INSERT INTO `sys_role_menu` VALUES (2, 1025);
INSERT INTO `sys_role_menu` VALUES (2, 1026);
INSERT INTO `sys_role_menu` VALUES (2, 1027);
INSERT INTO `sys_role_menu` VALUES (2, 1028);
INSERT INTO `sys_role_menu` VALUES (2, 1029);
INSERT INTO `sys_role_menu` VALUES (2, 1030);
INSERT INTO `sys_role_menu` VALUES (2, 1031);
INSERT INTO `sys_role_menu` VALUES (2, 1032);
INSERT INTO `sys_role_menu` VALUES (2, 1033);
INSERT INTO `sys_role_menu` VALUES (2, 1034);
INSERT INTO `sys_role_menu` VALUES (2, 1035);
INSERT INTO `sys_role_menu` VALUES (2, 1036);
INSERT INTO `sys_role_menu` VALUES (2, 1037);
INSERT INTO `sys_role_menu` VALUES (2, 1038);
INSERT INTO `sys_role_menu` VALUES (2, 1039);
INSERT INTO `sys_role_menu` VALUES (2, 1040);
INSERT INTO `sys_role_menu` VALUES (2, 1041);
INSERT INTO `sys_role_menu` VALUES (2, 1042);
INSERT INTO `sys_role_menu` VALUES (2, 1043);
INSERT INTO `sys_role_menu` VALUES (2, 1044);
INSERT INTO `sys_role_menu` VALUES (2, 1045);
INSERT INTO `sys_role_menu` VALUES (2, 1046);
INSERT INTO `sys_role_menu` VALUES (2, 1047);
INSERT INTO `sys_role_menu` VALUES (2, 1048);
INSERT INTO `sys_role_menu` VALUES (2, 1049);
INSERT INTO `sys_role_menu` VALUES (2, 1050);
INSERT INTO `sys_role_menu` VALUES (2, 1051);
INSERT INTO `sys_role_menu` VALUES (2, 1052);
INSERT INTO `sys_role_menu` VALUES (2, 1053);
INSERT INTO `sys_role_menu` VALUES (2, 1054);
INSERT INTO `sys_role_menu` VALUES (2, 1055);
INSERT INTO `sys_role_menu` VALUES (2, 1056);
INSERT INTO `sys_role_menu` VALUES (2, 1057);
INSERT INTO `sys_role_menu` VALUES (2, 1058);
INSERT INTO `sys_role_menu` VALUES (2, 1059);
INSERT INTO `sys_role_menu` VALUES (2, 1060);
INSERT INTO `sys_role_menu` VALUES (100, 1);
INSERT INTO `sys_role_menu` VALUES (100, 2);
INSERT INTO `sys_role_menu` VALUES (100, 3);
INSERT INTO `sys_role_menu` VALUES (100, 100);
INSERT INTO `sys_role_menu` VALUES (100, 101);
INSERT INTO `sys_role_menu` VALUES (100, 102);
INSERT INTO `sys_role_menu` VALUES (100, 103);
INSERT INTO `sys_role_menu` VALUES (100, 104);
INSERT INTO `sys_role_menu` VALUES (100, 105);
INSERT INTO `sys_role_menu` VALUES (100, 106);
INSERT INTO `sys_role_menu` VALUES (100, 107);
INSERT INTO `sys_role_menu` VALUES (100, 108);
INSERT INTO `sys_role_menu` VALUES (100, 109);
INSERT INTO `sys_role_menu` VALUES (100, 110);
INSERT INTO `sys_role_menu` VALUES (100, 111);
INSERT INTO `sys_role_menu` VALUES (100, 112);
INSERT INTO `sys_role_menu` VALUES (100, 113);
INSERT INTO `sys_role_menu` VALUES (100, 114);
INSERT INTO `sys_role_menu` VALUES (100, 115);
INSERT INTO `sys_role_menu` VALUES (100, 116);
INSERT INTO `sys_role_menu` VALUES (100, 500);
INSERT INTO `sys_role_menu` VALUES (100, 501);
INSERT INTO `sys_role_menu` VALUES (100, 1001);
INSERT INTO `sys_role_menu` VALUES (100, 1002);
INSERT INTO `sys_role_menu` VALUES (100, 1003);
INSERT INTO `sys_role_menu` VALUES (100, 1004);
INSERT INTO `sys_role_menu` VALUES (100, 1005);
INSERT INTO `sys_role_menu` VALUES (100, 1006);
INSERT INTO `sys_role_menu` VALUES (100, 1007);
INSERT INTO `sys_role_menu` VALUES (100, 1008);
INSERT INTO `sys_role_menu` VALUES (100, 1009);
INSERT INTO `sys_role_menu` VALUES (100, 1010);
INSERT INTO `sys_role_menu` VALUES (100, 1011);
INSERT INTO `sys_role_menu` VALUES (100, 1012);
INSERT INTO `sys_role_menu` VALUES (100, 1013);
INSERT INTO `sys_role_menu` VALUES (100, 1014);
INSERT INTO `sys_role_menu` VALUES (100, 1015);
INSERT INTO `sys_role_menu` VALUES (100, 1016);
INSERT INTO `sys_role_menu` VALUES (100, 1017);
INSERT INTO `sys_role_menu` VALUES (100, 1018);
INSERT INTO `sys_role_menu` VALUES (100, 1019);
INSERT INTO `sys_role_menu` VALUES (100, 1020);
INSERT INTO `sys_role_menu` VALUES (100, 1021);
INSERT INTO `sys_role_menu` VALUES (100, 1022);
INSERT INTO `sys_role_menu` VALUES (100, 1023);
INSERT INTO `sys_role_menu` VALUES (100, 1024);
INSERT INTO `sys_role_menu` VALUES (100, 1025);
INSERT INTO `sys_role_menu` VALUES (100, 1026);
INSERT INTO `sys_role_menu` VALUES (100, 1027);
INSERT INTO `sys_role_menu` VALUES (100, 1028);
INSERT INTO `sys_role_menu` VALUES (100, 1029);
INSERT INTO `sys_role_menu` VALUES (100, 1030);
INSERT INTO `sys_role_menu` VALUES (100, 1031);
INSERT INTO `sys_role_menu` VALUES (100, 1032);
INSERT INTO `sys_role_menu` VALUES (100, 1033);
INSERT INTO `sys_role_menu` VALUES (100, 1034);
INSERT INTO `sys_role_menu` VALUES (100, 1035);
INSERT INTO `sys_role_menu` VALUES (100, 1036);
INSERT INTO `sys_role_menu` VALUES (100, 1037);
INSERT INTO `sys_role_menu` VALUES (100, 1038);
INSERT INTO `sys_role_menu` VALUES (100, 1039);
INSERT INTO `sys_role_menu` VALUES (100, 1040);
INSERT INTO `sys_role_menu` VALUES (100, 1041);
INSERT INTO `sys_role_menu` VALUES (100, 1042);
INSERT INTO `sys_role_menu` VALUES (100, 1043);
INSERT INTO `sys_role_menu` VALUES (100, 1044);
INSERT INTO `sys_role_menu` VALUES (100, 1045);
INSERT INTO `sys_role_menu` VALUES (100, 1046);
INSERT INTO `sys_role_menu` VALUES (100, 1047);
INSERT INTO `sys_role_menu` VALUES (100, 1048);
INSERT INTO `sys_role_menu` VALUES (100, 1049);
INSERT INTO `sys_role_menu` VALUES (100, 1050);
INSERT INTO `sys_role_menu` VALUES (100, 1051);
INSERT INTO `sys_role_menu` VALUES (100, 1052);
INSERT INTO `sys_role_menu` VALUES (100, 1053);
INSERT INTO `sys_role_menu` VALUES (100, 1054);
INSERT INTO `sys_role_menu` VALUES (100, 1055);
INSERT INTO `sys_role_menu` VALUES (100, 1056);
INSERT INTO `sys_role_menu` VALUES (100, 1057);
INSERT INTO `sys_role_menu` VALUES (100, 1058);
INSERT INTO `sys_role_menu` VALUES (100, 1059);
INSERT INTO `sys_role_menu` VALUES (100, 1060);

-- ----------------------------
-- Table structure for sys_user
-- ----------------------------
DROP TABLE IF EXISTS `sys_user`;
CREATE TABLE `sys_user`  (
  `user_id` bigint(0) NOT NULL AUTO_INCREMENT COMMENT '用户ID',
  `dept_id` bigint(0) NULL DEFAULT NULL COMMENT '部门ID',
  `user_name` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '用户账号',
  `nick_name` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '用户昵称',
  `user_type` varchar(2) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '00' COMMENT '用户类型（00系统用户）',
  `email` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '' COMMENT '用户邮箱',
  `phonenumber` varchar(11) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '' COMMENT '手机号码',
  `sex` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '0' COMMENT '用户性别（0男 1女 2未知）',
  `avatar` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '' COMMENT '头像地址',
  `password` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '' COMMENT '密码',
  `status` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '0' COMMENT '帐号状态（0正常 1停用）',
  `del_flag` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '0' COMMENT '删除标志（0代表存在 2代表删除）',
  `login_ip` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '' COMMENT '最后登录IP',
  `login_date` datetime(0) NULL DEFAULT NULL COMMENT '最后登录时间',
  `create_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`user_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 100 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci COMMENT = '用户信息表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of sys_user
-- ----------------------------
INSERT INTO `sys_user` VALUES (1, 100, 'admin', '清眸物联', '00', 'tsingeye@company.com', '15888888888', '0', '/tsingeye-viid/2022/04/19/blob_20220419103206A001.jpeg', '$2a$10$Pfz1W41gpFRzfkRtmarSjOuCzH2vvII4PcBUu8SP3scuvpiqYuAl2', '0', '0', '27.128.27.192', '2022-09-06 17:06:48', 'admin', '2022-04-15 10:31:07', 'admin', '2022-09-06 17:06:48', '管理员');
INSERT INTO `sys_user` VALUES (2, 102, 'test1', '测试用户1', '00', 'test@qq.com', '15666666666', '1', '/tsingeye-iot/2022/04/15/blob_20220415113120A001.jpeg', '$2a$10$ISzZTrjd9NefIofCy40/auJw0PMwftsTTrg.g2ExJz/2YIqMzY5Ie', '0', '0', '127.0.0.1', '2022-04-15 11:31:07', 'admin', '2022-04-15 10:31:07', 'admin', '2022-04-15 11:31:19', '测试员');

-- ----------------------------
-- Table structure for sys_user_post
-- ----------------------------
DROP TABLE IF EXISTS `sys_user_post`;
CREATE TABLE `sys_user_post`  (
  `user_id` bigint(0) NOT NULL COMMENT '用户ID',
  `post_id` bigint(0) NOT NULL COMMENT '岗位ID',
  PRIMARY KEY (`user_id`, `post_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci COMMENT = '用户与岗位关联表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of sys_user_post
-- ----------------------------
INSERT INTO `sys_user_post` VALUES (1, 1);
INSERT INTO `sys_user_post` VALUES (2, 4);

-- ----------------------------
-- Table structure for sys_user_role
-- ----------------------------
DROP TABLE IF EXISTS `sys_user_role`;
CREATE TABLE `sys_user_role`  (
  `user_id` bigint(0) NOT NULL COMMENT '用户ID',
  `role_id` bigint(0) NOT NULL COMMENT '角色ID',
  PRIMARY KEY (`user_id`, `role_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci COMMENT = '用户和角色关联表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of sys_user_role
-- ----------------------------
INSERT INTO `sys_user_role` VALUES (1, 1);
INSERT INTO `sys_user_role` VALUES (2, 100);

-- ----------------------------
-- Table structure for viid_device
-- ----------------------------
DROP TABLE IF EXISTS `viid_device`;
CREATE TABLE `viid_device`  (
  `id` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '主键',
  `device_id` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '设备id',
  `register_time` timestamp(0) NULL DEFAULT NULL COMMENT '最后一次注册时间',
  `keep_alive_time` timestamp(0) NULL DEFAULT NULL COMMENT '最后一次保活时间',
  `status` int(0) NULL DEFAULT NULL COMMENT '在线状态 1、在线 2、不在线',
  `enable` int(0) NULL DEFAULT 1 COMMENT '启用状态 1、启用  2、不启用',
  `ip` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '设备ip',
  `dict_conver` int(0) NULL DEFAULT NULL COMMENT '字典转换（1、开，2、关）dictzhuanhuan',
  `format_clean` int(0) NULL DEFAULT NULL COMMENT '格式清洗（1、开，2、关）geshiqingxi',
  `encrypt_push` int(0) NULL DEFAULT NULL COMMENT '加密推送（1、开，2、关）jiamituisong',
  `decrypt_push` int(0) NULL DEFAULT NULL COMMENT '解密推送（1、开，2、关）jiemituisong',
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '点位全称',
  `longitude` decimal(65, 8) NULL DEFAULT NULL COMMENT '经度',
  `latitude` decimal(65, 8) NULL DEFAULT NULL COMMENT '纬度',
  `police` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '派出所',
  `type` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '类型',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '设备信息' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of viid_device
-- ----------------------------

-- ----------------------------
-- Table structure for viid_face
-- ----------------------------
DROP TABLE IF EXISTS `viid_face`;
CREATE TABLE `viid_face`  (
  `id` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '主键',
  `face_id` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '人脸标识',
  `info_kind` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '信息分类：人工采集、自动采集',
  `source_id` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '来源标识：来源图像信息标识',
  `device_id` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '设备编码',
  `shot_time` datetime(0) NULL DEFAULT NULL COMMENT '拍摄时间',
  `left_top_x` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '左上角X坐标',
  `left_top_y` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '左上角Y坐标',
  `right_btm_x` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '右下角X坐标',
  `right_btm_y` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '右下角Y坐标',
  `location_mark_time` datetime(0) NULL DEFAULT NULL COMMENT '位置标记时间',
  `face_appear_time` datetime(0) NULL DEFAULT NULL COMMENT '人脸出现时间',
  `face_dis_appear_time` datetime(0) NULL DEFAULT NULL COMMENT '人脸消失时间',
  `gender_code` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '性别代码',
  `age_up_limit` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '年龄上限',
  `age_lower_limit` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '年龄下限',
  `glass_style` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '眼镜款式',
  `emotion` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '不清楚字段含义',
  `is_driver` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '是否驾驶员 0：否、1：是、2：不确定',
  `is_foreigner` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '是否涉外人员 0：否、1：是、2：不确定',
  `is_suspected_terrorist` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '是否涉恐人员 0：否、1：是、2：不确定',
  `is_criminal_involved` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '是否涉案人员 0：否、1：是、2：不确定',
  `is_detainess` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '是否在押人员 0：否、1：是、2：不确定',
  `is_victim` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '是否被害人 0：否、1：是、2：不确定',
  `is_suspicious_person` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '是否可疑人员 0：否、1：是、2：不确定',
  `similaritydegree` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '相似度[0,1]',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `remark` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci COMMENT = '人脸信息' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of viid_face
-- ----------------------------

-- ----------------------------
-- Table structure for viid_face_image
-- ----------------------------
DROP TABLE IF EXISTS `viid_face_image`;
CREATE TABLE `viid_face_image`  (
  `id` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '编号',
  `face_id` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '人脸标识',
  `image_id` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '图像标识',
  `event_sort` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '事件分类：自动分析事件类型，设备采集必选',
  `device_id` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '设备编码',
  `storage_path` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '存储路径：图像文件的存储路径，采用URI命名规则',
  `type` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '不清楚自动含义',
  `file_format` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '图像文件格式',
  `shot_time` datetime(0) NULL DEFAULT NULL COMMENT '拍摄时间',
  `width` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '宽度',
  `height` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '高度',
  `image_url` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT 'minio上传后的图片地址',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '上传时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci COMMENT = '人脸照片信息' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of viid_face_image
-- ----------------------------

-- ----------------------------
-- Table structure for viid_motor_vehicle
-- ----------------------------
DROP TABLE IF EXISTS `viid_motor_vehicle`;
CREATE TABLE `viid_motor_vehicle`  (
  `ID` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '主键',
  `MotorVehicleID` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `InfoKind` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `SourceID` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `TollgateID` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `DeviceID` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `StorageUrl1` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `StorageUrl2` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `StorageUrl3` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `StorageUrl4` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `StorageUrl5` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `LeftTopX` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `LeftTopY` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `RightBtmX` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `RightBtmY` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `LaneNo` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `HasPlate` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `PlateClass` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `PlateColor` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `PlateNo` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `PlateNoAttach` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `PlateDescribe` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `IsDecked` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `IsAltered` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `IsCovered` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `Speed` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `Direction` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `DrivingStatusCode` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `UsingPropertiesCode` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `VehicleClass` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `VehicleBrand` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `VehicleModel` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `VehicleStyles` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `VehicleLength` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `VehicleWidth` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `VehicleHeight` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `VehicleColor` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `VehicleColorDepth` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `VehicleHood` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `VehicleTrunk` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `VehicleWheel` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `WheelPrintedPattern` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `VehicleWindow` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `VehicleRoof` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `VehicleDoor` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `SideOfVehicle` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `CarOfVehicle` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `RearviewMirror` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `VehicleChassis` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `VehicleShielding` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `FilmColor` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `IsModified` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `HitMarkInfo` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `VehicleBodyDesc` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `VehicleFrontItem` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `DescOfFrontItem` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `VehicleRearItem` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `DescOfRearItem` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `NumOfPassenger` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `PassTime` datetime(0) NULL DEFAULT NULL,
  `NameOfPassedRoad` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `IsSuspicious` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `PlateReliability` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `PlateCharReliability` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `BrandReliability` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `AppearTime` datetime(0) NULL DEFAULT NULL,
  `Calling` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `DisappearTime` datetime(0) NULL DEFAULT NULL,
  `MarkTime` datetime(0) NULL DEFAULT NULL,
  `Sunvisor` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `SafetyBelt` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `create_time` datetime(0) NULL DEFAULT NULL,
  PRIMARY KEY (`ID`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '机动车信息' ROW_FORMAT = COMPACT;

-- ----------------------------
-- Records of viid_motor_vehicle
-- ----------------------------

-- ----------------------------
-- Table structure for viid_motor_vehicle_image
-- ----------------------------
DROP TABLE IF EXISTS `viid_motor_vehicle_image`;
CREATE TABLE `viid_motor_vehicle_image`  (
  `ID` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '编号',
  `MotorVehicleID` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '车辆标识符',
  `ImageID` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '图像标识',
  `EventSort` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '事件分类',
  `DeviceID` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '设备编码',
  `StoragePath` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '存储路径',
  `Type` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '不清楚字段含义',
  `FileFormat` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '图像文件格式',
  `ShotTime` datetime(0) NULL DEFAULT NULL COMMENT '拍摄时间',
  `Width` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '宽度',
  `Height` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '高度',
  `ImageUrl` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT 'minio上传后的图片地址',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '上传时间',
  PRIMARY KEY (`ID`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci COMMENT = '机动车图片' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of viid_motor_vehicle_image
-- ----------------------------

-- ----------------------------
-- Table structure for viid_person
-- ----------------------------
DROP TABLE IF EXISTS `viid_person`;
CREATE TABLE `viid_person`  (
  `ID` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '主键',
  `PersonID` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '人员标识',
  `InfoKind` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '信息分类',
  `SourceID` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '来源标识',
  `DeviceID` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '设备编码',
  `LeftTopX` int(0) NULL DEFAULT NULL COMMENT '左上角X坐标',
  `LeftTopY` int(0) NULL DEFAULT NULL COMMENT '左上角Y坐标',
  `RightBtmX` int(0) NULL DEFAULT NULL COMMENT '右下角X坐标',
  `RightBtmY` int(0) NULL DEFAULT NULL COMMENT '右下角Y坐标',
  `PersonAppearTime` datetime(0) NULL DEFAULT NULL COMMENT '人体出现时间',
  `LocationMarkTime` datetime(0) NULL DEFAULT NULL COMMENT '位置标记时间',
  `PersonDisAppearTime` datetime(0) NULL DEFAULT NULL COMMENT '人员消失时间',
  `IDType` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '证件种类',
  `IDNumber` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '证件号码',
  `Name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '姓名',
  `UsedName` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '曾用名',
  `Alias` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '绰号',
  `GenderCode` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '性别代码',
  `AgeUpLimit` int(0) NULL DEFAULT NULL COMMENT '年龄上限',
  `AgeLowerLimit` int(0) NULL DEFAULT NULL COMMENT '年龄下限',
  `EthicCode` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '民族代码',
  `NationalityCode` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '国籍代码',
  `NativeCityCode` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '籍贯省市县代码',
  `ResidenceAdminDivision` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '居住地行政区划',
  `ChineseAccentCode` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '汉语口音代码',
  `PersonOrg` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '单位名称',
  `JobCategory` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '职业类别代码',
  `AccompanyNumber` int(0) NULL DEFAULT NULL COMMENT '同行人员',
  `HeightUpLimit` int(0) NULL DEFAULT NULL COMMENT '身高上限',
  `HeightLowerLimit` int(0) NULL DEFAULT NULL COMMENT '身高下限',
  `BodyType` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '体型',
  `SkinColor` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '肤色',
  `HairStyle` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '发型',
  `HairColor` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '发色',
  `Gesture` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '姿态',
  `Status` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '状态',
  `FaceStyle` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '脸型',
  `FacialFeature` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '脸部特征',
  `PhysicalFeature` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '体貌特征',
  `BodyFeature` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '体表特征',
  `HabitualMovement` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '习惯动作',
  `Behavior` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '行为',
  `BehaviorDescription` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '行为描述',
  `Appendant` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '附属物',
  `AppendantDescription` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '附属物描述',
  `UmbrellaColor` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '伞颜色',
  `RespiratorColor` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '口罩颜色',
  `CapStyle` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '帽子款式',
  `CapColor` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '帽子颜色',
  `GlassStyle` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '眼镜款式',
  `GlassColor` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '眼镜颜色',
  `ScarfColor` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '围巾颜色',
  `BagStyle` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '包款式',
  `BagColor` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '包颜色',
  `CoatStyle` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '上衣款式',
  `CoatLength` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '上衣长度',
  `CoatColor` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '上衣颜色',
  `TrousersStyle` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '裤子款式',
  `TrousersColor` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '裤子颜色',
  `TrousersLen` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '裤子长度',
  `ShoesStyle` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '鞋子款式',
  `ShoesColor` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '鞋子颜色',
  `IsDriver` int(0) NULL DEFAULT NULL COMMENT '是否驾驶员',
  `IsForeigner` int(0) NULL DEFAULT NULL COMMENT '是否涉外人员',
  `PassportType` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '护照证件种类',
  `ImmigrantTypeCode` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '出入境人员类别代码',
  `IsSuspectedTerrorist` int(0) NULL DEFAULT NULL COMMENT '是否涉恐人员',
  `SuspectedTerroristNumber` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '涉恐人员编号',
  `IsCriminalInvolved` int(0) NULL DEFAULT NULL COMMENT '是否涉案人员',
  `CriminalInvolvedSpecilisationCode` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '涉案人员专长代码',
  `BodySpeciallMark` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '体表特殊标记',
  `CrimeMethod` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '作案手段',
  `CrimeCharacterCode` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '作案特点代码',
  `EscapedCriminalNumber` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '在逃人员编号',
  `IsDetainees` int(0) NULL DEFAULT NULL COMMENT '是否在押人员',
  `DetentionHouseCode` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '看守所编码',
  `DetaineesIdentity` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '在押人员身份',
  `DetaineesSpecialIdentity` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '在押人员特殊身份',
  `MemberTypeCode` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '成员类型代码',
  `IsVictim` int(0) NULL DEFAULT NULL COMMENT '是否被害人',
  `VictimType` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '被害人种类',
  `InjuredDegree` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '受伤害程度',
  `CorpseConditionCode` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '尸体状况代码',
  `IsSuspiciousPerson` int(0) NULL DEFAULT NULL COMMENT '是否可疑人',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '上传时间',
  PRIMARY KEY (`ID`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci COMMENT = '人体信息' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of viid_person
-- ----------------------------

-- ----------------------------
-- Table structure for viid_person_image
-- ----------------------------
DROP TABLE IF EXISTS `viid_person_image`;
CREATE TABLE `viid_person_image`  (
  `ID` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '主键',
  `PersonID` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '人体标识',
  `ImageUrl` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '图片地址',
  `ImageID` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '图片标识',
  `EventSort` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `DeviceID` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '设备编号',
  `StoragePath` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `Type` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '未知类型',
  `FileFormat` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '文件格式',
  `ShotTime` datetime(0) NULL DEFAULT NULL,
  `Width` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '宽度',
  `Height` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '高度',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '上传时间',
  PRIMARY KEY (`ID`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci COMMENT = '人体图片' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of viid_person_image
-- ----------------------------

SET FOREIGN_KEY_CHECKS = 1;
